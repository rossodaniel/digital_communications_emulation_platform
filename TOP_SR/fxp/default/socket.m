classdef socket < handle 
properties
	host='127.0.0.1' 
	port=5806 
	bridge
end
	methods
		function obj = socket();
			obj.bridge = tcpip(obj.host, obj.port,'NetworkRole', 'client');
			obj.bridge.OutputBufferSize=500000;
			obj.bridge.InputBufferSize=500000;
			fopen(obj.bridge);
			pause(0.2);
			fwrite(obj.bridge, "!");
		end
	end
end
