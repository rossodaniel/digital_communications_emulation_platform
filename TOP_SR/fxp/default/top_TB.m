%%%%%%%%%%%%%%%TEST ENVIROMENT%%%%%%%%%%%%%%%
close all
clear all
clc

%Change work directory
cd('/home/fer/PPS/TOP_SR/fxp/default/')
%Simulation variables
L=2; 
P=1;


%CallObject
top_obj=top();

%CallSocketObject
socket_obj=socket();

%Receive data
L_i_seed=fread(socket_obj.bridge,1,'double');
while(L_i_seed>0)
	i_seed=fread(socket_obj.bridge,L_i_seed,'double');
	i_seed=reshape(i_seed,1,[]);
	top_obj.seti_seed(i_seed);

	L_i_kernel=fread(socket_obj.bridge,1,'double');
	i_kernel=fread(socket_obj.bridge,L_i_kernel,'double');
	i_kernel=reshape(i_kernel,1,[]);
	top_obj.seti_kernel(i_kernel);

	L_i_valid=fread(socket_obj.bridge,1,'double');
	i_valid=fread(socket_obj.bridge,L_i_valid,'double');
	i_valid=reshape(i_valid,1,[]);
	top_obj.seti_valid(i_valid);

	L_i_reset=fread(socket_obj.bridge,1,'double');
	i_reset=fread(socket_obj.bridge,L_i_reset,'double');
	i_reset=reshape(i_reset,1,[]);
	top_obj.seti_reset(i_reset);
	%RunMethod
	top_obj.run();

	%SendData
	o_error=top_obj.geto_error();

	for i=1:length(o_error)
		fwrite(socket_obj.bridge, o_error (i),'double')
	end
	o_data_slicer=top_obj.geto_data_slicer();

	for i=1:length(o_data_slicer)
		fwrite(socket_obj.bridge, o_data_slicer (i),'double')
	end
	o_data=top_obj.geto_data();

	for i=1:length(o_data)
		fwrite(socket_obj.bridge, o_data (i),'double')
	end

	%SlaveMethod
	top_obj.slave();
	L_i_seed=fread(socket_obj.bridge,1,'double');

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%$$$
