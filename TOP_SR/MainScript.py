import socket
import struct
import binascii
from string import *
import shutil
import subprocess
import time 
import os
import random
import inspect
from pprint import pprint
import math
import re

class mathematicable(object):
    def truncation (self,number,NBF):
        return(math.floor(number*2**NBF)/2**NBF)
    
    def intEq (self,number,NBF):
        return(int(math.floor(number*2**NBF)))

    def rounding (self,number,NBF):
        return(math.floor((number+2**-(NBF+1))*2**NBF)/2**NBF)
    
    def saturation(self,number,NB,NBF): 
        return((number>2**(NB-NBF-1)-2**-NBF) * (2**(NB-NBF-1)-2**-NBF -number) + (number<-2**(NB-NBF-1))*(-2**(NB-NBF-1)-number) + number)


class matlabeable(object):  
    
    def __init__(self):
        self.matlabPath=self.mainPath+"/fxp/"
        
        try:
            os.stat(self.matlabPath)
        except:
            os.mkdir(self.matlabPath) 

        #ToDo: Compare string with attributesString and create the folder with differences. 
        
        #self.matlabPath+=self.attributesString+"/"
        self.matlabPath+="default/"#"m415_n2520/"#"m5000_n30000/"#default/"#"m415_n2520/"
        try:
            os.stat(self.matlabPath)
        except:
            os.mkdir(self.matlabPath)  

    def createMatlabBridge(self):
        file = open(self.matlabPath+"socket.m", "w")
        stringToFile="classdef socket < handle \n"
        stringToFile+="properties\n"
        stringToFile+="\thost='%s' \n"% (self.host)
        stringToFile+="\tport=%s \n"% (self.port)
        stringToFile+="\tbridge\n"
        stringToFile+="end\n"
        stringToFile+="\tmethods\n"
        stringToFile+="\t\tfunction obj = socket();\n"
        stringToFile+="\t\t\tobj.bridge = tcpip(obj.host, obj.port,'NetworkRole', 'client');\n"
        stringToFile+="\t\t\tobj.bridge.OutputBufferSize=500000;\n"
        stringToFile+="\t\t\tobj.bridge.InputBufferSize=500000;\n"
        stringToFile+="\t\t\tfopen(obj.bridge);\n"
        stringToFile+="\t\t\tpause(0.2);\n"
        stringToFile+="\t\t\tfwrite(obj.bridge, \"!\");\n"
        stringToFile+="\t\tend\n"
        stringToFile+="\tend\n"
        stringToFile+="end\n"
        file.write(stringToFile)
        
    def openMatlab(self):
        os.system('export PATH=$PATH:/usr/local/MATLAB/R2017b/bin')
        print("Abriendo Matlab")
        matlabPathRun="< %s%s_TB.m" % (self.matlabPath,self.data.attributes['name'])
        print(matlabPathRun)
        subprocess.Popen(['matlab', '-nodisplay -nosplash', matlabPathRun])
    
    def createMatlabTestBench(self,P=1,L=2,nameOfDut=3):
        firstInputPort=0 #To check L_A for close socket
        stringHeader="%%%%%%%%%%%%%%%TEST ENVIROMENT%%%%%%%%%%%%%%%\n"
        stringHeader+="close all\nclear all\nclc\n\n"
        stringHeader+="%Change work directory\n"
        stringHeader+="cd('%s')\n" % (self.matlabPath)
        #stringHeader+="p=genpath('../../../../');"
        #stringHeader+="addpath(p);"
        #stringHeader+="addpath('../libs/LDPC_ParityCheckMatrixHandler/');"
        

        stringHeader+="%Simulation variables\n"
        stringHeader+="L=%s; \nP=%s;\n\n\n" % (L,P)
        stringHeader+="%CallObject\n"
        stringHeader+="%s_obj=%s();\n\n" % (self.data.attributes['name'],self.data.attributes['name'])
        stringHeader+="%CallSocketObject\n"
        stringHeader+="socket_obj=socket();\n\n"  #If socketeable and matlabeable?
        stringHeader+="%Receive data\n"
        
        for i in self.data.attributes['ports']:
            if(i['type']=="input"):
                stringHeader+="L_%s=fread(socket_obj.bridge,1,'double');\n" % (i['name'])
                break
            
        stringHeader+="while(L_%s>0)\n" % (i['name'])
        
        for i in self.data.attributes['ports']:
            if(i['type']=="input"):
                if(firstInputPort>0):
                    stringHeader+="\n\tL_%s=fread(socket_obj.bridge,1,'double');\n" % (i['name'])    
                stringHeader+="\t%s=fread(socket_obj.bridge,L_%s,'double');\n" % (i['name'],i['name'])
                stringHeader+="\t%s=reshape(%s,1,[]);\n" % (i['name'],i['name'])
                stringHeader+="\t%s_obj.set%s(%s);\n" % (self.data.attributes['name'],i['name'],i['name'])
                
                firstInputPort=1         

        stringHeader+="\t%RunMethod\n"
        stringHeader+="\t%s_obj.run();\n" % (self.data.attributes['name'])  

        stringHeader+="\n\t%SendData\n"
        

        for i in self.data.attributes['ports']:
            if((i['type']=="output") or (i['type']=="internal")):
                stringHeader+="\t%s=%s_obj.get%s();\n\n" % (i['name'],self.data.attributes['name'],i['name'])
                stringHeader+="\tfor i=1:length(%s)\n" % (i['name'])
                stringHeader+="\t\tfwrite(socket_obj.bridge, %s (i),'double')\n" % (i['name'])
                stringHeader+="\tend\n";
                #stringHeader+="\n\t%String Finish\n";
                #stringHeader+="\tfprintf(socket_obj.bridge, \"%s\",\'#');\n"
                #stringHeader+="\t\tfwrite(socket_obj.bridge, \"#\",'double')\n"
        
        stringHeader+="\n\t%SlaveMethod"
        stringHeader+="\n\t%s_obj.slave();\n" % (self.data.attributes['name'])
        for i in self.data.attributes['ports']:   
            if(i['type']=="input"):
                stringHeader+="\tL_%s=fread(socket_obj.bridge,1,'double');\n" % (i['name'])
                break


         
        stringHeader+="\nend\n\n";
        stringHeader+="%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n"
        stringHeader+="%$$$\n"
        
        stringTestBench=""
        try:
            f = open(self.matlabPath+self.data.attributes['name']+"_TB.m","r+")
            d = f.readlines()
            flag=0
            for i in d:
                if(flag==1):
                    stringTestBench+=i

                if i == '%$$$\n':
                    flag=1
        except:
            f = open(self.matlabPath+self.data.attributes['name']+"_TB.m","w")
            f.close()
       
        f = open(self.matlabPath+self.data.attributes['name']+"_TB.m","w")
        f.write(stringHeader) #Add header to file
        f.write(stringTestBench) #Add header to file
        f.close() #CloseFile

    def createMatlabModule(self):
        #Si el archivo no existe, se crea
        stringProperties=""
        stringCustomConstructor=""
        
        strBeginMark="%==========="
        strEndMark="===========%"

        strBeginAtogenProperties=strBeginMark+"BEGIN AUTOGENERATED PROPERTIES"+strEndMark
        strBeginCustomProperties=strBeginMark+"BEGIN CUSTOM PROPERTIES"+strEndMark
        strEndCustomProperties=strBeginMark+"END CUSTOM PROPERTIES"+strEndMark
        strBeginAutogenMethods=strBeginMark+"BEGIN AUTOGENERATED METHODS"+strEndMark
        strBeginCustomConstructor=strBeginMark+"CUSTOM CONSTRUCTOR"+strEndMark
        strEndCustomConstructor=strBeginMark+"END CUSTOM CONSTRUCTOR"+strEndMark
        strBeginCustomMethods=strBeginMark+"BEGIN CUSTOM METHODS"+strEndMark
        strEndMethods=strBeginMark+"FINISH-METHODS"+strEndMark

        try:
            f = open(self.matlabPath+self.data.attributes['name']+".m","r+")
            d = f.readlines()
            stringMethods=""
            flagP=flagM=flagC=0
            
            for i in d:
                if(flagP==1):
                    if (len(re.findall(r''+strEndCustomProperties,i))):
                        flagP=0 
                    else:
                        stringProperties+=i;

                if(flagC==1):
                    if (len(re.findall(r''+strEndCustomConstructor,i))):
                        flagC=0 
                    else:
                        stringCustomConstructor+=i;
                
                if(flagM==1):
                    if (len(re.findall(r''+strEndMethods,i))):
                        stringMethods+=i;
                        stringMethods+="\tend\nend";      
                        break
                    else:
                        stringMethods+=i;
                
                if (len(re.findall(r''+strBeginCustomProperties,i))==1):
                    flagP=1
                    #print("P=1")
                if (len(re.findall(r''+strBeginCustomMethods,i))==1):
                    flagM=1
                    #print("M=1")
                if (len(re.findall(r''+strBeginCustomConstructor,i))==1):
                    flagC=1
                   #print("C=1")

            f.close()
        except IOError as e:
            #El archivo no existe:
            f = open(self.matlabPath+self.data.attributes['name']+".m","w")
            #RunMethod
            stringMethods="\t \tfunction run (%s)\n\t \tend\n \n" % (self.data.attributes['name'])
            #ResetMethod
            stringMethods+="\t \tfunction reset (%s)\n\t \tend\n\n" % (self.data.attributes['name'])
            #SlaveMethod
            stringMethods+="\t \tfunction slave (%s)\n\t \tend\n\n" % (self.data.attributes['name'])
            stringMethods+="\t"+strEndMethods+"\n"
            stringMethods+="\tend\nend";
            f.close()
        except:
            print "Error inesperado", sys.exc_info()[0]
            raise
            
            
        stringHeader="classdef %s < handle\n \tproperties\n" % (self.data.attributes['name'])
        stringHeader+="\t"+strBeginAtogenProperties+"\n"
        for i in self.data.attributes['ports']:
            if(i['name'].count(".")==0): #No remote internal
                stringHeader+="\n \t %s;\n" % ((i['name']))
                if(i['parallelism']!=1):
                    stringHeader+="\t P_%s=%s;\n" % (str.upper(i['name']),i['parallelism'])
            
                if(i['n_bits']!=1):
                    stringHeader+="\t NB_%s=%s;\n" % (str.upper(i['name']),i['n_bits'])
                if('nf_bits' in i.keys()):
                    stringHeader+="\t NBF_%s=%s;\n" % (str.upper(i['name']),i['nf_bits'])
                stringHeader+="\t SIGN_%s=%s;\n" % (str.upper(i['name']),i['sign'])
            #if(i['log']==1):
                #stringHeader+='\t LOG_%s="%s%s.dat";\n' % (i['name'],self.data.attributes['path'],i['name'])
        
        stringHeader+="\t"+strBeginCustomProperties+"\n"
        stringHeader+=stringProperties
        stringHeader+="\t"+strEndCustomProperties+"\n"
        stringHeader+="\tend\n"
        stringHeader+="\tmethods\n"
        stringHeader+="\t"+strBeginAutogenMethods+"\n"
        stringHeader+="\t \tfunction obj = %s()\n" % (self.data.attributes['name'])
        
        #Master and Slave Structure
        for i in self.data.attributes['ports']:
            if((i['type']=="output") or (("internal" in i['type']) and (i['name'].count(".")==0))): #Check if output and local internal. If the strings have dots
                if('registered' in i.keys() and i['registered']):
                    stringHeader+="\t \t \t obj.%s=struct('master',zeros(1,(obj.P_%s-1)),'slave',zeros(1,(obj.P_%s-1)));\n" % (i['name'],str.upper(i['name']),str.upper(i['name']))
                
        #stringHeader+="\t \tend\n";
        stringHeader+="\n\t\t\t"+strBeginCustomConstructor+"\n"
        stringHeader+=stringCustomConstructor
        stringHeader+="\t\t\t"+strEndCustomConstructor+"\n"
        stringHeader+="\t\tend\n"

        #Setters&Getters
        for i in self.data.attributes['ports']:
            if(i['type']!="input"):
                if(i['name'].count(".")>0): #Remote internal
                    split=i['name'].split(".")
                    name=split[-1]
                    
                    stringHeader+="\t \tfunction [%s] = get%s(%s,%s);\n" % (name,name,self.data.attributes['name'],name)
                    if('registered' in i.keys() and i['registered']):
                        stringHeader+="\t \t \t %s=%s.%s.slave;\n" % (name,self.data.attributes['name'],i['name'])
                    else:
                        stringHeader+="\t \t \t %s=%s.%s;\n" % (name,self.data.attributes['name'],i['name'])
                else: #normal output or internal
                    stringHeader+="\t \tfunction [%s] = get%s(%s,%s);\n" % (i['name'],i['name'],self.data.attributes['name'],i['name'])
                    if('registered' in i.keys() and i['registered']):
                        stringHeader+="\t \t \t %s=%s.%s.slave;\n" % (i['name'],self.data.attributes['name'],i['name'])
                    else:
                        stringHeader+="\t \t \t %s=%s.%s;\n" % (i['name'],self.data.attributes['name'],i['name'])


                
            else: #input
                stringHeader+="\t \tfunction [%s] = set%s(%s,%s)\n" % (i['name'],i['name'],self.data.attributes['name'],i['name'])
                stringHeader+="\t \t \t %s.%s=%s;\n" % (self.data.attributes['name'],i['name'],i['name'])
            
            stringHeader+="\t \tend\n";
        

        stringHeader+="\t"+strBeginCustomMethods+"\n"
        stringHeader+=stringMethods;  
        #stringHeader+="\tend\nend";      
    
        f = open(self.matlabPath+self.data.attributes['name']+".m", 'w')
        f.write(stringHeader)
        #f.write(stringModule)
        f.close()

class socketeable(mathematicable):
    host = "127.0.0.1"
    port = random.randrange(5000, 6000, 1)
    mySocket= None 
    conn=None
    addr=None
    putDataDict={}
    #def __init__(self):
        
       

    def openSocket(self):
        print("Abriendo socket")
        self.mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.mySocket.settimeout(None)
        self.mySocket.bind((self.host,self.port)) 
        self.mySocket.listen(1)
        self.conn, self.addr = self.mySocket.accept()
        print("\nServidor iniciado correctamnete. Esperando Conexion\n")
        data=""
        while data!='!':
            data = self.conn.recv(8)
        print("Matlab se conectado correctamente\n")
    
        for i in self.data.attributes['ports']:           
            f = open(self.rtlPath+"dataTo"+ i['name'] + ".dat", 'w') #ClearFiles
            f.close()

    def putData(self,Port,Data,quantized=1):
        founded=0
        dataReady=list()
        for i in self.data.attributes['ports']:
            if(Port==i['name']):
                if(quantized==1):
                    for j in Data:
                        j=self.rounding(j,i['nf_bits'])
                        j=self.saturation(j,i['n_bits'],i['nf_bits'])
                        dataReady.append(j)
                else:
                    dataReady=Data
                founded=1
                self.putDataDict[Port]=dataReady
                #break cuando termine de buscar     
        
        if(founded==0):
            print("Datos no almacenados, el puerto " + Port + "NO HA SIDO ENCONTRADO")    
    

    def run(self):
        #print inspect.stack()
        iterationCounter=0    
        for i in self.data.attributes['ports']:
            if(i['type']=="input"):
                data=self.putDataDict[i['name']]
                if self.addr is not None:
                    self.conn.send(struct.pack("!d", len(data))) #Cantidad de datos a enviar
                    
                else:
                    print("No esta conectado con  Matlab")
                    break
                f = open(self.rtlPath+"dataTo"+i['name']+ ".dat", 'a') #openToAppend
            
                for jj in data:
                    if('nf_bits' in i.keys()):
                        if(i['nf_bits'] != 0):
                            f.write(str(int(self.intEq(jj,i['nf_bits']))))
                        else:
                            f.write(str(int(jj)))
                    else:
                        f.write(str(int(jj)))
                    f.write(" ")
                    iterationCounter+=1
                    if(iterationCounter==i['parallelism']):
                        f.write("\n")
                        iterationCounter=0

                    if self.addr is not None:
                        self.conn.send(struct.pack("!d", jj)) # 32-bit integer in network byte order      
                    else:      
                        print("No esta conectado con  Matlab")
                        break


    def closeSocket(self):
        self.conn.send(struct.pack("!d", 0)) 
        #self.conn.send("0") 
        print("Cerrando Socket desde Python")
        self.mySocket.shutdown(1)
        self.mySocket.close()

    def receiveData(self):
        print("Recibiendo datos...")
        dataReceived = {}
        for i in self.data.attributes['ports']:
            if("output"==i['type'] or "internal"==i['type']):
                print("\nRecibiendo datos del puerto: " + i['name'])
                buffer=list()
                f = open(self.rtlPath+"dataTo"+i['name']+ ".dat", 'a') #openToAppend
                 #ToDo: Change for another algorithm of find.
                name=i['name']
                parallelism=i['parallelism']
                nbf=i['nf_bits']

                for i in range(0,parallelism):
                    data = self.conn.recv(8) #Tipo de dato
                    data=((struct.unpack("!d", data)[0]))
                    f.write(str(self.intEq(data,nbf)))
                    f.write(" ")
                    buffer.append(data)
                f.write("\n")        
                f.close()
                dataReceived[name]=buffer;                                        
        return(dataReceived)


    def logData(self,Port,Data):         
        for i in self.data.attributes['ports']:
            if(Port==i['name']):
                #ToDo: Change for another algorithm of find
                parallelism=i['parallelism']

        f = open("dataTo"+ Port + ".dat", 'w') #l
        iterationCounter=0

        for i in Data:
            f.write(str(i))
            f.write(" ")
            iterationCounter+=1
            if(iterationCounter==parallelism):
                f.write("\n")
                iterationCounter=0


class documentable(object):
    pass
"""
    from pylatex import Document, Section, Subsection, Command
    from pylatex.utils import italic, NoEscapeF
    doc = Document()

    def title(self,title,author):
        self.doc.preamble.append(self.Command('title', title))
        self.doc.preamble.append(self.Command('author', author))
        self.doc.preamble.append(self.Command('date', self.NoEscape(r'\today')))
        self.doc.append(self.NoEscape(r'\maketitle'))
    
    def section(self,title,text):
        with self.doc.create(self.Section(title)):
            self.doc.append(text)

    def generate(self):
        self.doc.generate_pdf('pylatex_example_output')
"""


class rtleable (object):
    def __init__(self):
        self.rtlPath=self.mainPath+"/rtl/"
        try:
            os.stat(self.rtlPath)
        except:
            os.mkdir(self.rtlPath) 

        #self.rtlPath+=self.attributesString+"/"
        self.rtlPath+="default/"#"m415_n2520/"#"m5000_n30000/"#"default/"#"m415_n2520/"
        #print(self.rtlPath)
        try:
            os.stat(self.rtlPath)
        except:
            os.mkdir(self.rtlPath)
            
    def createVerilogModule(self):
        #***GENERACION DEL MODULO***#
        #print(self.matlabPath)
        #=self.mainPath+"/system/"
        #Si existe salvamos la parte del codigo
        stringModule=""
        try:
            f = open(self.rtlPath+self.data.attributes['name']+".v","r+")
            d = f.readlines()
            flag=0
            for i in d:
                if(flag==1):
                    stringModule+=i

                if i == '/*$$$*/\n':
                    flag=1

        except:
            #si no existe creamos un archivo vacio
            f = open(self.rtlPath+self.data.attributes['name']+".v","w")
            f.close()
            

        file = open(self.rtlPath+self.data.attributes['name']+".v", "w")

        # Cabecera del archivo
        stringHeader="module %s \n" % (self.data.attributes['name'])
        stringHeader+="#(\n"
        
        for i in self.data.attributes['ports']:
            if(i['type']!="internal"):
                if(i['parallelism']!=1):
                    stringHeader+= "\tparameter PARALELLISM_%s =%d,\n" % (str.upper(i['name']), i['parallelism'])

                if(i['n_bits']!=1):
                    stringHeader+= "\tparameter NB_%s ='d%d,\n" % (str.upper(i['name']), i['n_bits'])

                if 'nf_bits' in i.keys():
                    if(i['nf_bits']!=0):
                        stringHeader += "\tparameter NBF_%s ='d%d,\n" % (str.upper(i['name']), i['nf_bits'])
                
        stringHeader = stringHeader[:-2] #Remove last ,
        

           
        stringHeader+= "\n)\n"

        # Definicion de puertos
        stringHeader+='(\n'

        # borrar la siguiente linea
        stringHeader+="\tinput \tclk,\n"

        iterationCounter = 0
        for i in self.data.attributes['ports']:
            
            if(i['sign']==0):
                stringSign=""
            else:
                stringSign="signed"

            if(i['type']=="input" or i['type']=="output"): #si no es entrada es salida o internal
                stringHeader+= "\t"
                if(i['parallelism']==1):
                    if(i['n_bits']!=1):
                        stringHeader+= "%s %s \t[NB_%s-1:0] %s,\n" % (i['type'],stringSign,str.upper(i['name']), i['name'])
                    else:
                        stringHeader+= "%s %s %s,\n" % (i['type'],stringSign,i['name'])
                else:
                    if(i['n_bits']!=1):
                        stringHeader+= "%s %s \t[PARALELLISM_%s*NB_%s-1:0] %s,\n" % (i['type'], stringSign ,str.upper(i['name']), str.upper(i['name']), i['name'])
                    else:
                        stringHeader+= "%s %s \t[PARALELLISM_%s-1:0] %s,\n" % (i['type'],stringSign,str.upper(i['name']), i['name'])
            
        stringHeader = stringHeader[:-2] #Remove last ,
        stringHeader+='\n);\n'
        stringHeader+='\n/*$$$*/\n'

        if("endmodule" not in stringModule):
            stringHeader+='\nendmodule\n'

        
        file.write(stringHeader)
        file.write(stringModule)
        file.close()

    def createVerilogTestBench(self):
           
        try:
            f = open(self.rtlPath+self.data.attributes['name']+"_TB.v","r+")
            d = f.readlines()
            stringModule=""
            flag=0
            for i in d:
                if(flag==1):
                    stringModule+=i

                if i == '/*$$$*/\n':
                    flag=1



        except:
            #si no existe creamos un archivo vacio
            f = open(self.rtlPath+self.data.attributes['name'] + "_TB.v","w")
            f.close()
        
        #***TEST_BENCH***#
        # Apertura del archivo
        file = open(self.rtlPath+self.data.attributes['name']+"_TB.v", "w")

        # Cabecera del archivo
        stringHeader="module " + self.data.attributes['name'] + "_TB();" + "\n\n"


        # Definicion de parametros

        for i in self.data.attributes['ports']:
            if(i['n_bits']!=1):
                stringHeader += "localparam NB_%s ='d%d;\n" % (str.upper(i['name']), i['n_bits'])
            
            if 'nf_bits' in i.keys():
                if(i['nf_bits']!=0):
                    stringHeader += "localparam NBF_%s ='d%d;\n" % (str.upper(i['name']), i['nf_bits'])
            
            if(i['parallelism']!=1):
                stringHeader += "localparam PARALELLISM_%s =%d;" % (str.upper(i['name']), i['parallelism'])
            
            
            stringHeader += "\n\n"
            

        # Declaracion de variables para apertura de archivos

        stringHeader+="\n/*Variables para la apertura de archivos*/\n"

        # Seniales de exitacion
        for i in self.data.attributes['ports']:
            if "input" == i['type'] or "internal" == i['type']:
                stringHeader += "integer testFile%s;\n" % (i['name'])
                stringHeader += "integer linesReaded%s;\n" % (i['name'])
                stringHeader += "integer inputTo%s;\n\n" % (i['name'])

        # Seniales de salida
            if 'log' in i.keys():
                if(i['log']):
                    stringHeader += "integer logOut%s;\n" % (i['name'])


            # Seniales para vector match
            if 'match' in i.keys():
                if(i['match']):
                    stringHeader += "integer fileToMatch%s;\n" % (i['name'])
                    stringHeader += "integer linesReadedMatch%s;\n" % (i['name'])
                    stringHeader += "integer dataCompare%s;\n\n" % (i['name'])
 

        stringHeader+="\n/*Entradas y salidas del modulo*/\n"

        # Entradas y salidas del modulo
        for i in self.data.attributes['ports']:
            if(i['sign']==0):
                stringSign=""
            else:
                stringSign="signed "

            if("input" == i['type'] or "internal" == i['type']):
                stringHeader += "reg "
                stringHeader+=stringSign
            else:
                stringHeader += "wire "
                stringHeader+=stringSign

            if(i['parallelism']==1):
                if(i['n_bits']!=1):
                    stringHeader += "[NB_%s-1:0] %s;\n" % (str.upper(i['name']), i['name'])
                else:
                    stringHeader += "%s;\n" % (i['name'])
            else:
                if(i['n_bits']!=1):
                    stringHeader += "[PARALELLISM_%s*NB_%s-1:0] %s;\n" % (str.upper(i['name']), str.upper(i['name']), i['name'])
                else:
                    stringHeader += "[PARALELLISM_%s-1:0] %s;\n" % (str.upper(i['name']), i['name'])                    
        
        stringHeader+="\n/*Seniales internas de vector matching*/\n"

        # Entradas y salidas del modulo
        for i in self.data.attributes['ports']:
            
            if(i['sign']==0):
                stringSign=""
            else:
                stringSign="signed"

            if 'match' in i.keys():
                if(i['match']):
                    if(i['parallelism']==1):
                        if(i['n_bits']!=1):
                            stringHeader += "reg %s [NB_%s-1:0] match_%s;\n" % (stringSign,str.upper(i['name']),i['name'])
                        else:
                            stringHeader += "reg match_%s;\n" % (i['name'])
                    else:
                        if(i['n_bits']!=1):
                            stringHeader += "reg %s [PARALELLISM_%s*NB_%s-1:0] match_%s;\n" % (stringSign,str.upper(i['name']),str.upper(i['name']), i['name'])
                        else:
                            stringHeader += "reg [PARALELLISM_%s-1:0] match_%s;\n" % (str.upper(i['name']), i['name'])
                    stringHeader += "wire %s_assert;\n" % (i['name'])
         

        for i in self.data.attributes['ports']:
            if("internal" == i['type']):
                stringHeader += "wire %s_assert;\n" % (i['name'])


        stringHeader += "\n/*Variables propias del modelo*/\n"
        
        stringHeader += "\nreg clk=0; \ninteger i;\n"
            
        #stringHeader+="\n/*Vectores para la carga de datos*/\n"
        # Generacion de los LongVector (vector chorizo)
        #for i in self.data.attributes['ports']:
         #   if("input" == i['type']):
          #      stringHeader+= "reg [PARALELLISM_%s*NB_%s-1:0] LV%s;\n" % (
          #          i['name'], i['name'], i['name'])
                

                
        stringHeader+="\n/*Generacion de clock*/\n"
        
        # Generacion de Clock
        stringHeader += "\nalways\nbegin\n\t #%s clk = ~clk;\nend\n" % self.data.attributes['clocktimes']



        # Generacion del bloque initial
        stringHeader+= "\ninitial\nbegin\n"
        
        # Seniales de exitacion
        for i in self.data.attributes['ports']:
            if("input" == i['type'] or "internal" == i['type'] ):
                stringHeader+= "\ttestFile%s = $fopen(\"%sdataTo%s.dat\",\"r\");\n" % (i['name'], 
                                                          self.rtlPath,
                                                          i['name'])
                
                stringHeader+= '\tif(testFile%s=="NULL")\n' % (i['name'])

                stringHeader+= '\tbegin\n\t\t$display("Unable to open file");\n\t\t$stop;\n'
                
                stringHeader+= '\tend\n\n'
                
     
            if 'log' in i.keys():
                if(i['log']):
                    stringHeader+= "\tlogOut%s = $fopen(\"%sLog%s.dat\",\"w\");\n" % (i[
                        'name'], self.rtlPath,i['name'])
                    
                    stringHeader+= '\tif(logOut%s=="NULL")\n' % (i['name'])
                
                    stringHeader+= '\tbegin\n\t\t$display("Unable to open file");\n\t\t$stop;\n'
                    
                    stringHeader+= '\tend\n\n'
                   
            
            if 'match' in i.keys():
                if(i['match']):
                    stringHeader+= "\tfileToMatch%s = $fopen(\"%sdataTo%s.dat\",\"r\");\n" % (i['name'], self.rtlPath,i['name'])
                
                    stringHeader+= '\tif(fileToMatch%s=="NULL")\n' % (i['name'])
                
                    stringHeader+= '\tbegin\n\t\t$display("Unable to open file");\n\t\t$stop;\n'
                
                    stringHeader+= '\tend\n\n'
                
        # Cierre del ciclo principal
        stringHeader+= "end\n"
        

        # Ciclo principal
        stringHeader+= "\n\nalways @(posedge clk)\nbegin\n"
        
        # Carga de variables en cada Flop
        for i in self.data.attributes['ports']:
            if("input" == i['type'] or "internal" == i['type'] ):
                # Bloque para cargar toda la linea de datos
                if(i['parallelism']!=1):
                    stringHeader+= "\tfor(i=0;i<PARALELLISM_%s;i=i+1)\n" % (str.upper(i['name']))
                    stringHeader+= "\tbegin\n"
                
                stringHeader+= "\t\tlinesReaded%s<= $fscanf(testFile%s,\"%s\",inputTo%s);\n" %(i['name'],i['name'],"%d",i['name'])
                stringHeader+= "\t\tif ($feof(testFile%s))\n" % (i['name'])
                stringHeader+= "\t\tbegin\n"
                stringHeader+= '\t\t\t$display("Fin del archivo"); \n'
                #stringHeader+= '\t\t\t$fclose(logOutC); \n'
                stringHeader+= '\t\t\t//CLOSE ALL FILES HERE\n'
                stringHeader+= "\t\t\t$finish;\n"
                stringHeader+= "\t\tend\n\n\n"
                
               # Armado del vector chorizo
                if(i['n_bits']!=1 and i['parallelism']!=1):
                    stringHeader+= "\t\t%s[(NB_%s*(i+1))-1-:NB_%s]<=(inputTo%s); \n" % (i['name'], str.upper(i['name']), str.upper(i['name']), i['name'])
                
                if(i['n_bits']==1 and i['parallelism']!=1):
                    stringHeader+= "\t\t%s[((i+1))-1-:1]<=(inputTo%s); \n" % (i['name'],i['name'])

                if(i['n_bits']==1 and i['parallelism']==1):
                    stringHeader+= "\t\t%s<=(inputTo%s); \n" % (i['name'], i['name'])
                    
                if(i['n_bits']!=1 and i['parallelism']==1):
                    if(i['sign']==1):
                        stringHeader+= "\t\t%s[NB_%s-1-:NB_%s]<=$signed(inputTo%s); \n" % (i['name'], str.upper(i['name']), str.upper(i['name']), i['name'])
                    else:
                        stringHeader+= "\t\t%s[NB_%s-1-:NB_%s]<=(inputTo%s); \n" % (i['name'], str.upper(i['name']), str.upper(i['name']), i['name'])
                
                if(i['parallelism']!=1):
                    stringHeader+= "\t\tend\n\n\n"
            if 'match' in i.keys():
                if(i['match']):
                # Bloque para cargar toda la linea de datos
                    if(i['parallelism']!=1):
                        stringHeader+= "\tfor(i=0;i<PARALELLISM_%s;i=i+1)\n" % (str.upper(i['name']))
                        stringHeader+= "\tbegin\n"
                    
                    stringHeader+= "\tlinesReadedMatch%s<= $fscanf(fileToMatch%s,\"%s\",dataCompare%s);\n" % (i['name'], i['name'], "%d", i['name'])
                    stringHeader+= "\n\tif ($feof(fileToMatch%s))\n"% (i['name'])
                    stringHeader+= "\t\tbegin\n"
                    stringHeader+= '\t\t\t$display("Fin del archivo"); \n'
                    stringHeader+= "\t\t\t$finish;\n"
                    stringHeader+= "\t\tend\n\n\n"
                    
                    #stringHeader+= "\t\tmatch_%s[(NB_%s*(i+1))-1-:NB_%s]=(dataCompare%s); \n" % (i['name'], str.upper(i['name']), str.upper(i['name']), i['name'])
                    
                         # Armado del vector chorizo
                    if(i['n_bits']!=1 and i['parallelism']!=1):
                        stringHeader+= "\t\tmatch_%s[(NB_%s*(i+1))-1-:NB_%s]<=(dataCompare%s); \n" % (i['name'], str.upper(i['name']), str.upper(i['name']), i['name'])
                
                    if(i['n_bits']==1 and i['parallelism']!=1):
                        stringHeader+= "\t\tmatch_%s[(i+1)-1-:1]<=(dataCompare%s); \n" % (i['name'], i['name'])

                    if(i['n_bits']==1 and i['parallelism']==1):
                        stringHeader+= "\t\tmatch_%s<=(dataCompare%s); \n" % (i['name'], i['name'])
                    
                    if(i['n_bits']!=1 and i['parallelism']==1):
                        stringHeader+= "\t\tmatch_%s[NB_%s-1-:NB_%s]<=(dataCompare%s); \n" % (i['name'], str.upper(i['name']), str.upper(i['name']), i['name'])

   
                    


                    if(i['parallelism']!=1):
                        stringHeader+= "\t\tend\n\n\n"
                
            if 'log' in i.keys():
                if(i['log']):
                    if(i['parallelism']!=1):
                        stringHeader+= "\tfor(i=0;i<PARALELLISM_%s;i=i+1)\n" % (str.upper(i['name']))
                        stringHeader+= "\tbegin\n"
                    else:
                        if(i['sign']):
                            stringHeader+= "\t \t $fwrite(logOut%s,\"%s\",$signed(%s[(i+1)*NB_%s-1-:NB_%s]));\n" % (i['name'], "%d", i['name'],str.upper(i['name']),str.upper(i['name']))
                        else:
                            stringHeader+= "\t \t $fwrite(logOut%s,\"%s\",%s[(i+1)*NB_%s-1-:NB_%s]);\n" % (i['name'], "%d\t", i['name'],str.upper(i['name']),str.upper(i['name']))


                    if(i['parallelism']!=1):
                        stringHeader+= "\t\tend\n\n\n"
                
                    stringHeader+="\t$fwrite ((logOut%s), \"\\n\");\n" % (i['name'])
        
        stringHeader+= "\n\nend"
        

        
        # Mapeo de seniales internas
        
        #for i in self.data.attributes['ports']:
        #    if("internal" == i['type']):
        #    stringHeader+=("\n \n")        
        
        # Instanciacion del modulo
        stringHeader+=("\n \n")
        
        stringHeader+="///***ASSERTS***///\n"
        for i in self.data.attributes['ports']:
            if(i['type']=="internal"):
                    stringHeader += "assign %s_assert = (%s==DUT_%s.%s) ? 1'b1 : 1'b0;\n" % (i['name'],i['name'],self.data.attributes['name'],i['name'])
            else:
                if 'match' in i.keys():
                    if(i['match']):
                        stringHeader += "assign %s_assert = (match_%s==%s) ? 1'b1 : 1'b0;\n" % (i['name'],i['name'],i['name'])
    
            

        stringHeader+="\n \n"
        stringHeader+=self.data.attributes['name']
        
        
        stringHeader+="#(\n"
        # Mapeo de parametros
       
        for i in self.data.attributes['ports']:
            if(i['type']!="internal"):
                if(i['n_bits']!=1):
                    stringHeader+= "\t.NB_%s \t \t \t(NB_%s),\n" % (str.upper(i['name']), str.upper(i['name']))
            
                if("nf_bits" in i.keys()):
                    if(i['nf_bits']!=0):
                        stringHeader+= "\t.NBF_%s \t \t \t(NBF_%s),\n" % (str.upper(i['name']), str.upper(i['name']))
            
        stringHeader = stringHeader[:-2] #Remove last ,

        stringHeader+= ")\n \n \n \tDUT_%s ( \n " % self.data.attributes['name']
        
       
        stringHeader+="\t.clk          (clk),\n"

        # Mapeo de puertos
        
        for i in self.data.attributes['ports']:
            if(i['type']!="internal"):
                if(i['parallelism']!=1):
                    if(i['n_bits']!=1):
                        stringHeader+= "\t.%s \t \t \t(%s[PARALELLISM_%s*NB_%s-1:0]),\n" % (i['name'], i['name'], str.upper(i['name']), str.upper(i['name']))
                    else:
                        stringHeader+= "\t.%s \t \t \t(%s[PARALELLISM_%s-1:0]),\n" % (i['name'], i['name'], str.upper(i['name']))
                else:
                    if(i['n_bits']!=1):
                        stringHeader+= "\t.%s \t \t \t(%s[NB_%s-1:0]),\n" % (i['name'], i['name'], str.upper(i['name']))                
                    else:
                        stringHeader+= "\t.%s \t \t \t(%s),\n" % (i['name'], i['name'])



        stringHeader = stringHeader[:-2] #Remove last ,

        stringHeader+= "\n \n);\n \nendmodule"
        file.write(stringHeader)
        file.close()
 
    def simRTLISE(self):
        currentPath= os.getcwd()
        file = open(self.rtlPath+"isim.cmd", "w")
        stringToFile="onerror {resume}\n"
        stringToFile+="wave add /\n"
        stringToFile+="run 1000 ns;"
        file.write(stringToFile)
        file.close()

        file = open(self.rtlPath+self.data.attributes['name'] + ".prj", "w")
               
        stringToFile="verilog work \""                     
        stringToFile+=self.data.attributes['name']
        stringToFile+=".v\"\n"
        stringToFile+="verilog work \""                     
        stringToFile+=self.data.attributes['name']
        stringToFile+="_TB.v\"\n"
        stringToFile+="verilog work \"/opt/Xilinx/14.7/ISE_DS/ISE//verilog/src/glbl.v\""
        file.write(stringToFile)
        file.close()

        file = open(self.rtlPath+"isim.sh", "w")
        #Borrado de simulacion anterior:
        stringToFile="rm -R %sisim\n" % (self.rtlPath)  
        
        #print("rm -R %sisim\n") % (self.rtlPath)
        #stringToFile=""
        
        stringToFile+="rm %sfuse.log %sfuseRelaunch.cmd %sfuse.xmsgs %sisim.log %s%s.exe\n" % (self.rtlPath,self.rtlPath,self.rtlPath,self.rtlPath,self.rtlPath,self.data.attributes['name'])                                                                                                                         
        
        #Variables de entorno:
        stringToFile+="export PATH=$PATH:/opt/Xilinx/14.7/ISE_DS/ISE/bin/lin64\n"
        stringToFile+="export XILINX=/opt/Xilinx/14.7/ISE_DS\n"
        stringToFile+="export PLATFORM=lin\n"
        stringToFile+="export PATH=$PATH:$\\{XILINX}/bin/$\\{PLATFORM}\n"
        stringToFile+="export LD_LIBRARY_PATH=$\\{XILINX}/lib/$\\{PLATFORM}\n"
        
        #Variables de vlogcomp:
        stringToFile+="vlogcomp -work isim_temp -intstyle ise -prj %s.prj\n" % (self.data.attributes['name'])                                                                                                                         
        
        #Fuses:
        stringToFile+="fuse -intstyle ise -incremental -lib unisims_ver -lib unimacro_ver -lib xilinxcorelib_ver -o %s.exe -prj %s.prj work.%s work.%s_TB\n" % (self.data.attributes['name'],self.data.attributes['name'],self.data.attributes['name'],self.data.attributes['name'])                                                                                                                         
        
        #stringToFile+="fuse -intstyle ise -incremental -lib unisims_ver -lib unimacro_ver -lib xilinxcorelib_ver -lib secureip -o multiplicador.exe -prj multiplicador.prj work.multiplicador work.multiplicador_TB work.glbl\n"

        stringToFile+="./%s.exe -intstyle ise -gui -tclbatch isim.cmd" % (self.data.attributes['name'])    
        file.write(stringToFile)
        file.close()
        os.system('chmod 755 ' + self.rtlPath+"isim.sh")
        os.chdir(self.rtlPath+"/")
        retval = os.getcwd()
        os.system("sh isim.sh")
        os.chdir(currentPath)
        


    def CreateVivadoTclProperties(self):
        file = open(self.rtlPath+"vivado.tcl", "w")
        stringToFile='proc create_report { reportName command } {\n'
        stringToFile+='set status "."\n'
        stringToFile+='append status $reportName ".fail"\n'
        stringToFile+='if { [file exists $status] } {\n'
        stringToFile+='eval file delete [glob $status]\n'
        stringToFile+='}\n'
        stringToFile+='send_msg_id runtcl-4 info "Executing : $command"\n'
        stringToFile+='set retval [eval catch { $command } msg]\n'
        stringToFile+='if { $retval != 0 } {\n'
        stringToFile+='set fp [open $status w]\n'
        stringToFile+='close $fp\n'
        stringToFile+='send_msg_id runtcl-5 warning "$msg"\n'
        stringToFile+='}\n'
        stringToFile+='}\n\n'
        stringToFile+='create_project -in_memory -part xc7k70tfbg676-2\n'
        stringToFile+='set_param project.singleFileAddWarning.threshold 0\n'
        stringToFile+='set_param project.compositeFile.enableAutoGeneration 0\n'
        stringToFile+='set_param synth.vivado.isSynthRun true\n'
        stringToFile+='set_property webtalk.parent_dir %sTest.cache/wt [current_project]\n'  % (self.rtlPath)
        stringToFile+='set_property parent.project_path %sTest.xpr [current_project]\n'  % (self.rtlPath)
        stringToFile+='set_property default_lib xil_defaultlib [current_project]\n'
        stringToFile+='set_property target_language Verilog [current_project]\n'
        stringToFile+='set_property ip_output_repo %sTest.cache/ip [current_project]\n' % (self.rtlPath)
        stringToFile+='set_property ip_cache_permissions {read write} [current_project]\n'
        stringToFile+='read_verilog -library xil_defaultlib {\n'
        stringToFile+='%s%s.v\n' % (self.rtlPath,self.data.attributes['name'])
        stringToFile+='%s%s_TB.v\n' % (self.rtlPath,self.data.attributes['name'])
        stringToFile+='}\n\n'
        stringToFile+='foreach dcp [get_files -quiet -all -filter file_type=="Design\ Checkpoint"] {\n'
        stringToFile+='set_property used_in_implementation false $dcp}\n'
        stringToFile+='set_param ips.enableIPCacheLiteLoad 0\n'
        stringToFile+='close [open __synthesis_is_running__ w]\n'
        stringToFile+='synth_design -top %s_TB -part xc7k70tfbg676-2\n' % (self.data.attributes['name'])
        stringToFile+='set_param constraints.enableBinaryConstraints false\n'
        stringToFile+='write_checkpoint -force -noxdef %s_TB.dcp\n' % (self.data.attributes['name'])
        stringToFile+='create_report "synth_1_synth_report_utilization_0" "report_utilization -file %s_TB_utilization_synth.rpt -pb %s_TB_utilization_synth.pb"\n' % (self.data.attributes['name'],self.data.attributes['name'])
        stringToFile+='file delete __synthesis_is_running__\n'
        stringToFile+='close [open __synthesis_is_complete__ w]\n'
        file.write(stringToFile)
        file.close()


    def simRTLVivado(self,openGui=1):
        self.CreateVivadoTclProperties()
        currentPath= os.getcwd()
        file = open(self.rtlPath+"script.tcl", "w")
        stringToFile="create_project VivadoRTL -force\n"
        stringToFile+="add_files -norecurse {%s%s.v %s%s_TB.v}\n" % (self.rtlPath,self.data.attributes['name'],self.rtlPath,self.data.attributes['name']) 
        stringToFile+="update_compile_order -fileset sources_1\n"
        stringToFile+="reset_run synth_1\n"
        stringToFile+="launch_runs synth_1 -jobs 4\n"
        if openGui==1:
            stringToFile+="start_gui\n"
        
        stringToFile+="source %svivado.tcl\n" % (self.rtlPath)
        stringToFile+="launch_simulation\n"

        file.write(stringToFile)
        file.close()
        vivadoPathRun="%sscript.tcl" % (self.rtlPath)
        subprocess.Popen(['vivado', '-mode', 'batch',  '-source',vivadoPathRun])
	   









class parseable (object):
#pip install yamlordereddictloader
#pip install pyyaml

    attributes=None
    f=None

    import yaml
    import yamlordereddictloader
    #def loadData(self,file):
    def __init__ (self,file):    
        with open(file) as self.f:
            self.attributes = self.yaml.load(self.f, Loader=self.yamlordereddictloader.Loader)
            print("parseable")
class mutable (object):

    #ToDo: Change for another algorithm of find.
    #ToDo: Check if the port exist.

    def modifyPortN_bits(self,port,n_bits):
        for i in self.data.attributes['ports']:
            if(port==i['name']):
                #ToDo: Change for another algorithm of find
                i['n_bits']=n_bits
    #ToDo: Change for another algorithm of find.
    #ToDo: Check if the port exist.

    def modifyPortNf_bits(self,port,nf_bits):
        for i in self.data.attributes['ports']:
            if(port==i['name']):
                i['nf_bits']=nf_bits


