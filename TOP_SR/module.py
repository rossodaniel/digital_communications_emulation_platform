import MainScript
import os
import sys

class customModule (MainScript.socketeable,MainScript.documentable,MainScript.matlabeable,MainScript.rtleable,MainScript.mutable,MainScript.parseable):
    def __init__(self,file):
        self.data = MainScript.parseable(file) #Parseable instatiation to parse yml
        self.mainPath=os.getcwd()   #Main directory to use inimp
        self.attributesString=""
        #for i in self.data.attributes['ports']:
         #   if(i['type']!="internal"):
          #      self.attributesString+= "%s_NB%s_NF%s_P%s-" % (i['name'],i['n_bits'],i['nf_bits'],i['parallelism'])

        self.attributesString = self.attributesString[:-1] #Removemos la ultima coma
        
        if isinstance(self, MainScript.rtleable):
            MainScript.rtleable.__init__(self)
            self.createVerilogTestBench()
            self.createVerilogModule()

        if isinstance(self, MainScript.matlabeable): 
            MainScript.matlabeable.__init__(self)
            self.createMatlabModule()
            self.createMatlabTestBench()
         
        if isinstance(self, MainScript.socketeable): 
            MainScript.socketeable.__init__(self)
            self.createMatlabBridge()
            
        #ToDo: Upload this section to constructor
        if isinstance(self, MainScript.documentable): 
            docPath=self.mainPath+"/doc"
            try:
                os.stat(docPath)
            except:
                os.mkdir(docPath)  

    #ToDo: Change for another algorithm of find.
    #ToDo: Check if the port exist.
    #ToDo: Check if the attribute exist.
    def getPortAtrributes(self,port,attribute):
        for i in self.data.attributes['ports']:
            if(port==i['name']):
                return(i[attribute])
