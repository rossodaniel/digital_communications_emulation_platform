import unittest
import module
import numpy
import random
from scipy import signal as sg
import pylab as py
import pylab as py
import math


class test_firFilter(unittest.TestCase,module.customModule):

    def golden_ref(self, x, h, L):
        y = sg.convolve(x,h)
        return y[:L]

    def floating_model(self, x, valid, L):
        y = py.zeros(L)
        if(valid==1):
            for i in range(0,L):
                if (x[i]==1): #Este if tendra sentido con el fxp
                    y[i]=1
                else:
                    y[i]=-1
	return y

    def setUp(self):
        self.mod = module.customModule("top.yml")

    def test_comunication(self):
        L = 2000

        #Este limite es (2^7-1)*10, que tiene que ver el size de la PRBS.
        z=0
        val = 1
        val_n = 0
        NS = self.mod.getPortAtrributes("i_data","parallelism")
        #Semilla hardcodeada.
        x = [0, 1, 0, 0, 1, 0, 1] #Para paralelismo 7 (PRBS7).
        #generacion kernel de canal
        h = [0.25, 1, 0.4, -0.1 , -0.3, -0.5, 0, 0.1, 0, 0]

        N = self.mod.getPortAtrributes("i_kernel","parallelism")
        NB_T = self.mod.getPortAtrributes("i_kernel","n_bits")
        NB_F = self.mod.getPortAtrributes("i_kernel","nf_bits")
        for i in range (0,N):
             h[i] = self.tru_sat(h[i], NB_T, NB_F)

        #Semilla random:
        #x = py.randn(NS)
        # for j in range (0,NS):
        #     if x[j] > 0.5:
        #         x[j] = 1
        #         #x[j]= self.tru_sat(x[j], 2, 0)
        #     else:
        #         x[j] = 0
        #         #x[j]= self.tru_sat(x[j], 2, 0)

        # y2 = self.floating_model(x, valid, L).tolist()

#Matlab generation
        self.mod.createMatlabBridge()
        self.mod.createMatlabModule()
        self.mod.openMatlab()

        self.mod.openSocket()

        self.mod.putData("i_seed" , x ,0)
        self.mod.putData("i_kernel" , h ,0)
        self.mod.putData("i_valid" , [val] ,0) #War valid

        #Test float vs implementation
        for i in range(L):
           if(i<100):
               self.mod.putData("i_reset" , [val] ,0)
           elif(i>2000 and i<2200):
               self.mod.putData("i_reset" , [val] ,0)
           else:
               self.mod.putData("i_reset" , [val_n] ,0)


           self.mod.run()
           o=self.mod.receiveData()
           print "Progreso", i
           # print "Input", x[i]
           print o['o_error']

        #self.assertAlmostEqual(o,y2[i],delta=0.0001)

        self.mod.closeSocket()

    def tru_sat(self,a,NB,NBF):                                  # Funcion para truncar y saturar un dato
        a = math.floor(a*(2**NBF)) / (2**NBF)
        a = (a>2**(NB-NBF-1)-2**-NBF) * (2**(NB-NBF-1)-2**-NBF -a) + (a<-2**(NB-NBF-1))*(-2**(NB-NBF-1)-a) + a
        return a

    def energia (self,x):                                        # calculo de la energia del vector x

        e = 0.0
        for i in range(len(x)):
            e = e + x[i]**2
        return e

    def snr (self,xf, xpf):                                      # relacion entre el filtro punto flotante y punto fijo

        xe = numpy.zeros(len(xf))
        for ptr in range (len(xe)):
            xe[ptr] = xf[ptr] - xpf[ptr]
        error_db = 10 * numpy.log10(self.energia(xf)/self.energia(xe))
        return error_db

if __name__ == '__main__':
    unittest.main()
#To Documentate: pydoc -w ./fir_UT.py
