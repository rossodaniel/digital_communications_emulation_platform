module mpr 
#(
	parameter NB_O_DATA ='d2
)
(
	input 	clk,
	input  i_data,
	input  i_reset,
	input  i_valid,
	output signed 	[NB_O_DATA-1:0] o_data
);

/*$$$*/
reg [NB_O_DATA-1:0] resultado;

always@(posedge clk)
begin: mapper_stage
    if(i_reset)
    resultado = 2'b00;
    
    else 
    begin if(i_valid) begin
            if(i_data)
                resultado = 2'b01;
            else 
                resultado = 2'b11;
            end
          else
            resultado = 2'b00;
end    
end

assign o_data = resultado;

endmodule
