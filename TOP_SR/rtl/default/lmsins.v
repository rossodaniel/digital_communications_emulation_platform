module lmsins
#(
	parameter NB_I_DATA ='d9,
	parameter NBF_I_DATA ='d6,
	parameter NB_I_ERROR ='d6,
	parameter NBF_I_ERROR ='d6,
	parameter PARALELLISM_O_DATA =31,
	parameter NB_O_DATA ='d13,
	parameter NBF_O_DATA ='d10,
	parameter PARALELLISM_O_REG =30,
    parameter NB_O_REG ='d9,
    parameter NBF_O_REG ='d6
)
(
	input 	clk,
	input signed 	[NB_I_DATA-1:0] i_data,
	input signed 	[NB_I_ERROR-1:0] i_error,
	input  i_reset,
	output signed 	[PARALELLISM_O_DATA*NB_O_DATA-1:0] o_data,
	output signed 	[PARALELLISM_O_REG*NB_O_REG-1:0] o_reg
);

/*$$$*/
//Resolución de kernel antes de redondeo y saturación:
localparam NB_HS = 23;
localparam NBF_HS = 20;
localparam PE_HS = NB_HS-NBF_HS;
localparam DESCARTE = NBF_HS-NBF_O_DATA;
//Valores constantes y otros:
localparam mitad = ((PARALELLISM_O_DATA - 1)/2);
localparam MU = 8; //Indica la potencia en que debe desplazarse hacia la derecha (-) -> immmer menor que 0.
localparam NB_MULT = NB_I_DATA + NB_I_ERROR;
localparam NBF_MULT = NBF_I_DATA + NBF_I_ERROR;
localparam DIF = NB_HS - NB_MULT;
integer i=0, ii=0, j=0, jj=0, k=0, l=0;

//Registros y variables
reg signed [NB_I_DATA-1:0] regresor [PARALELLISM_O_DATA-2:0];
reg signed [NB_HS-1:0] h_s [PARALELLISM_O_DATA-1:0];
wire signed [NB_HS-1:0] actual_sat [PARALELLISM_O_DATA-1:0];
reg signed [NB_MULT-1:0] mult [PARALELLISM_O_DATA-1:0];
reg signed [NB_HS:0] actual [PARALELLISM_O_DATA-1:0];//NB_HS porque necesitamos el de overflow, pues es el resultado de restar.
reg signed [NBF_HS-1:0] peso [PARALELLISM_O_DATA-1:0]; //Es 16:20, o sea, la longitud decimal del siguiente paso -> h_s
reg [PARALELLISM_O_DATA-1:0] sign;
wire signed [PARALELLISM_O_DATA*NBF_HS-1:0] peso_int ;
wire signed [PARALELLISM_O_DATA*NB_HS-1:0] hs_int;
genvar v1, v2;
//-------------------------------------------- INSTANCIAS ---------------------------------------------------//

generate
   for (v1=0; v1 < PARALELLISM_O_DATA; v1=v1+1)
   begin: proceso_instanciacion_rys
      rys #(
       .NB_I_DATA(NB_HS),
       .NBF_I_DATA(NBF_HS),
       .NB_O_DATA (NB_O_DATA),
       .NBF_O_DATA(NBF_O_DATA)
       )
      u_rys(
       .clk(clk),
       .i_data(h_s[PARALELLISM_O_DATA-1-v1]), //Aca es donde se cruzan para dar correctas referencias. VER NOTA ABAJO.
       .o_data(o_data[(NB_O_DATA*v1) +: NB_O_DATA])
      );
   end
endgenerate

generate
   for (v2=0; v2 < PARALELLISM_O_DATA; v2=v2+1)
   begin: proceso_instanciacion_sat
      sat #(
       .NB_I_DATA(NB_HS+1), //Ancho kernel presaturado +1 por overflow. Se supone NB_HS mayor que tamaño de peso.
       .NBF_I_DATA(NBF_HS),
       .NB_O_DATA(NB_HS),
       .NBF_O_DATA(NBF_HS)
        )
      u_sat(
       .clk(clk),
       .i_data(actual[v2]),
       .o_data(actual_sat[v2])
   );
   end
endgenerate

//------------------------------------------ COMPORTAMIENTO -------------------------------------------------//
//ETAPA DE RETRASOS DE MUESTRAS.
always@(posedge clk, posedge i_reset)
begin:sampleshift
        if(i_reset == 1) begin
            for(ii=0;ii<PARALELLISM_O_DATA-1;ii=ii+1) begin
                regresor[ii] <= {NB_I_DATA{1'b0}};
            end
        end
        else begin
            for(jj=0;jj<PARALELLISM_O_DATA-1;jj=jj+1) begin
                if(jj==0)
                    regresor[jj] <= i_data;
                else
                    regresor[jj] <= regresor[jj-1];
            end
        end
end
//ETAPA DE MULTIPLICACIÓN Y TOMA DEL SIGNO DEL PRODUCTO.
always@(*)
begin:multiplicacion
    for(j=0;j<PARALELLISM_O_DATA;j=j+1) begin
        if (j==PARALELLISM_O_DATA-1) begin //Multiplicacion correspondiente al h_s0. Recordar que P_I_D_ANT = P_O_D - 1 por el regresor.
            mult[j] = i_data*i_error;
            sign[j] = mult[j][NB_MULT-1];
            end
        else begin
            mult[(PARALELLISM_O_DATA-1)-1-j] = regresor[j]*i_error;//0 a 8, 9 a 17 (agarrando de a 9 = NB_I_DATA_ANT)
            sign[(PARALELLISM_O_DATA-1)-1-j] = mult[(PARALELLISM_O_DATA-1)-1-j][NB_MULT-1];
            end
    end
end

//SHIFT MANUAL PARA CREAR PESO.
always@(*)
begin:shiftmanual
    for(k=0;k<PARALELLISM_O_DATA;k=k+1) begin
        peso[k] = {{DIF{sign[k]}},mult[k]};
        //peso[k] = mult[k] >>> 8;
    end
end
//ASIGNACIÓN DEL H_S CON LOS VALORES SATURADOS DE ACTUAL.
always@(posedge clk, posedge i_reset)
begin:cargahs
    if(i_reset) begin
        for (l=0;l<PARALELLISM_O_DATA;l=l+1) begin
            if(l==mitad)
            //if(l==30)
                h_s[l] = {1'b1,{(PE_HS-1){1'b0}},{(NBF_O_DATA){1'b0}},1'b0,{(DESCARTE-1){1'b0}}};

            else
                h_s[l] = {NB_HS{1'b0}};
        end
    end
    else begin
        for (l=0;l<PARALELLISM_O_DATA;l=l+1) begin
            h_s[l] = actual_sat[l];
        end
    end
end
//RESTADO Y ACTUALIZACIÓN DE LOS VALORES DEL KERNEL.
always@(*)
begin:actualizacionkernel
    for (i=0;i<PARALELLISM_O_DATA;i=i+1) begin
        actual[i] = h_s[i] - peso[i];
    end
end
//NOTA: hasta aquí, todos los MSB de los vectores matrix corresponden al elemento h0 del kernel, es decir el que se multiplica por
//la muestra de entrada. Por lo tanto, el valor h_s[0] de ese vector corresponderia al h30 del kernel del fir.

//Señales de muestra y evaluación:
assign peso_int = {peso[0][14:0], peso[1][14:0], peso[2][14:0]};
//assign peso_int = {peso[0], peso[1], peso[2]};
assign hs_int = {h_s[0],h_s[1],h_s[2],h_s[3],h_s[4],h_s[5],h_s[6],h_s[7],h_s[8],h_s[9],h_s[10],h_s[11],h_s[12],h_s[13],h_s[14],h_s[15],h_s[16],h_s[17],h_s[18],h_s[19],h_s[20]};

assign o_reg = {regresor[29],regresor[28],regresor[27],regresor[26],regresor[25],regresor[24],regresor[23],regresor[22],regresor[21],regresor[20],regresor[19],regresor[18],regresor[17],regresor[16],regresor[15],regresor[14],regresor[13],regresor[12],regresor[11],regresor[10],regresor[9],regresor[8],regresor[7],regresor[6],regresor[5],regresor[4],regresor[3],regresor[2],regresor[1],regresor[0]};


endmodule
