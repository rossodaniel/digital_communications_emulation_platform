module top 
#(
	parameter PARALELLISM_I_SEED =7,
	parameter PARALELLISM_I_KERNEL =10,
	parameter NB_I_KERNEL ='d7,
	parameter NBF_I_KERNEL ='d6,
	parameter NB_O_ERROR ='d6,
	parameter NBF_O_ERROR ='d6,
	parameter NB_O_DATA ='d16
)
(
	input 	clk,
	input  	[PARALELLISM_I_SEED-1:0] i_seed,
	input signed 	[PARALELLISM_I_KERNEL*NB_I_KERNEL-1:0] i_kernel,
	input  i_valid,
	input  i_reset,
	output signed 	[NB_O_ERROR-1:0] o_error,
	output  o_data_slicer,
	output  	[NB_O_DATA-1:0] o_data
);

/*$$$*/
//Parámetros de definición de módulos:
//MAPPER:
localparam NB_O_DATA_MAPPER = 'd2;
localparam NBF_O_DATA_MAPPER = 'd0;

//FIR_CANAL:
localparam PARALLELISM_FIR_CHANNEL = PARALELLISM_I_KERNEL;
localparam NB_O_DATA_CANAL = 'd9;
localparam NBF_O_DATA_CANAL = 'd6;

//PRBS7_Rx

//ECUALIZADOR
localparam NB_I_DATA_EQU = 'd9;
localparam NBF_I_DATA_EQU = 'd6;
localparam NB_O_ERROR_EQU = 'd6;
localparam NBF_O_ERROR_EQU = 'd6;

//Señales (o cables) de interconexión:
wire s_prbstx_to_mapper;
wire signed [NB_O_DATA_MAPPER-1:0] s_mapper_to_fir_ch;
wire signed [NB_O_DATA_CANAL-1:0] s_canal_to_equ;
wire s_equ_to_check;
//wire s_prbs_to_check;
// ------------------------------------------ ARQUITECTURA MODULAR ------------------------------------------------//
// Instanciacina MAPPER:
 mpr #(
       .NB_O_DATA(NB_O_DATA_MAPPER)
      )
u_mpr (
          .clk(clk),
       .i_data(s_prbstx_to_mapper),
       .i_valid(i_valid),
       .i_reset(i_reset),
       .o_data(s_mapper_to_fir_ch)
);

// Instanciacia PRBS7_TX:
 ptx #(
       .PARALELLISM_I_DATA(PARALELLISM_I_SEED)
      )
u_ptx (
    .clk(clk),
    .i_data(i_seed),
    .i_carga(i_reset),
    .o_data(s_prbstx_to_mapper)
    //.o_data(s_prbs_to_check)
);

// instancia fir CANAL

fch #( .NB_I_DATA(NB_O_DATA_MAPPER),
	   .NBF_I_DATA(NBF_O_DATA_MAPPER),
	   .PARALELLISM_I_KERNEL(PARALELLISM_I_KERNEL),
	   .NB_I_KERNEL(NB_I_KERNEL),
	   .NBF_I_KERNEL(NBF_I_KERNEL),
	   .NB_O_DATA(NB_O_DATA_CANAL),
	   .NBF_O_DATA(NBF_O_DATA_CANAL)
    )
    u_fch(.clk(clk),
          .i_data(s_mapper_to_fir_ch),
          .i_reset(i_reset),
          .i_kernel(i_kernel),
          .o_data(s_canal_to_equ)
    );

// instancia ecualizador

equ #(.NB_I_DATA(NB_I_DATA_EQU),
	  .NBF_I_DATA(NBF_I_DATA_EQU),
	  .NB_O_ERROR(NB_O_ERROR_EQU),
	  .NBF_O_ERROR(NBF_O_ERROR_EQU)
	  )
u_equ (.clk(clk),
       .i_data(s_canal_to_equ),
       .i_reset(i_reset),
       .o_error(o_error),
       .o_data_slicer(s_equ_to_check)
       );

chk #(.NB_O_DATA(NB_O_DATA)
	   )
u_chk(.clk(clk),
      .i_data(~s_equ_to_check),
      //.i_data(s_prbs_to_check),
	  .i_reset(i_reset),
	  .i_valid(i_valid),
	  .o_data(o_data)
	);

	assign o_data_slicer = s_equ_to_check;


endmodule
