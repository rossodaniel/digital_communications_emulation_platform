module chk 
#(
	parameter NB_O_DATA ='d16
)
(
	input 	clk,
	input  i_data,
	input  i_reset,
	input  i_valid,
	output  	[NB_O_DATA-1:0] o_data
);

/*$$$*/
/*Parámetros y variables de la PRBS*/
integer bfbk= 5'd1;
integer i=0;


localparam PRBS_LEN = 7; //seleccion tamanio PRBS


//Cálculo del bit de realimentación variable

always@(posedge clk)
begin
    case (PRBS_LEN)
      5'd7: begin
                  bfbk=1;

               end
      5'd9: begin
                  bfbk=4;

               end
      5'd11: begin
                  bfbk=2;

               end
      5'd15: begin
                  bfbk=1;


               end
      5'd20: begin
                  bfbk=17;


               end
      5'd23: begin
                  bfbk=5;

               end
      5'd31: begin
                  bfbk=3;

               end
      default: begin
                  bfbk=1;

               end
    endcase
end

//Registros y señales
reg [PRBS_LEN-1:0] prbsrx = {PRBS_LEN{1'b0}};
reg [PRBS_LEN-1:0] sr_rx = {PRBS_LEN{1'b0}};
reg serial_out = 1'b0; //Bit de salida
reg serial_out_comp = 1'b0; //Bit de salida y comparacion
//reg fbk= 1'b0;
reg fbk = 1'b0;
reg prbs_in = 1'b0; //Indica en que bit va el valor de la realimentacion
reg prbs_in_sel = 1'b0;
reg comp = 1'b0;

//Comportamiento

//assign fbk = prbsrx[0]^prbsrx[bfbk]; //calculo de fbk

always @ (*)
begin: calculo_fbk
    comp = serial_out^serial_out_comp;
    serial_out = prbsrx[0];
    serial_out_comp = sr_rx[0];
    fbk = prbsrx[0]^prbsrx[bfbk];
end

//assign prbs_in = prbs_in_sel ? i_data : fbk; //mux que indica si la prbs recibe los datos de entrada o los datos ya realimentados

//assign serial_out = prbsrx[0];
//assign serial_out_comp = sr_rx[0];
//assign comp = serial_out^serial_out_comp;


always@(*)
begin
    if(prbs_in_sel)
        prbs_in = i_data;
    else
        prbs_in = fbk;
end

always @(posedge clk)
begin
      if (i_reset)
        begin
           prbsrx <= {PRBS_LEN{1'b0}};
           sr_rx <= {PRBS_LEN{1'b0}};
        end
      else if (i_valid)
         begin
            prbsrx  <=  {prbs_in, prbsrx[PRBS_LEN-1:1]};
            sr_rx <= {i_data, sr_rx[PRBS_LEN-1:1]};
         end
end

//FSM
localparam ADQ = 2'b00;
localparam CHK = 2'b01;
localparam LCK = 2'b10;
localparam UMBRAL = 5;
localparam check_samples = 100;


integer cont = 0;

integer smallcnt = 3'b0;
integer bigcnt = 7'b0;
integer chk_error = 7'b0;
reg [NB_O_DATA-1:0] error = {NB_O_DATA{1'b0}};

reg [1:0] state,next_state = ADQ;
reg [1:0] chk_ok = 2'b00; //el primer bit indica si se esta procesando, el segundo indica los saltos de la FSM

   always @(posedge clk)
   begin
      if (i_reset) begin
         state <= ADQ;
         error <= {NB_O_DATA{1'b0}};
      end

      else if(i_valid) begin
         state <= next_state;
      end
   end

  //control
  always @ (posedge clk)
    begin: control_smallcnt
        if(i_valid)
            begin
            if(~i_reset)
                begin
                if(state == ADQ)
                    smallcnt = smallcnt+1;
                else
                    smallcnt = 0;
                end
            end

           else smallcnt = 0;
    end

   always @ (posedge clk)
    begin: control_bigcnt
        if(i_valid)
            begin
            if(state == ADQ)
            begin
                bigcnt = 0;
                chk_ok <= 2'b00;
            end

            else
            begin
                if(bigcnt == check_samples)
                    begin
                    if(chk_error <= UMBRAL)
                        begin
                        chk_ok <= 2'b11;
                        bigcnt <= 0;
                        chk_error <= 0;
                        end
                    else
                        begin
                        chk_ok <= 2'b10;
                        bigcnt <= 0;
                        chk_error <= 0;
                        end
                    end

                else
                    begin
                    if(chk_error <= UMBRAL)
                        begin
                        chk_ok <= 2'b01;
                        bigcnt <= bigcnt+1;
                        if(comp)
                            begin
                            chk_error <= chk_error+1;
                            if(state == LCK)
                                begin
                                error <= error + 1;
                                end
                            else
                                begin
                                error <= error;
                                end
                            end
                        else
                            begin
                            chk_error <= chk_error;
                            end
                        end

                    else
                        begin
                        chk_ok <= 2'b00;
                        bigcnt = bigcnt+1;
                        if(comp)
                           begin
                           chk_error <= chk_error+1;
                           if(state == LCK)
                               begin
                               error <= error + 1;
                               end
                           else
                               begin
                               error <= error;
                               end
                         end

                        else
                           begin
                           chk_error <= chk_error;

                           end
                        end
                    end
                end
             end
          end



   always @ (state, smallcnt, chk_ok)
      begin: logica_estado_siguiente
        case(state)
            ADQ: if(smallcnt == PRBS_LEN)
                    begin
                    next_state <= CHK;
                    end

                 else
                    begin
                    next_state <= ADQ;
                    end

            CHK: if(chk_ok == 2'b11)
                    begin
                    next_state <= LCK;
                    end

                 else if (chk_ok == 2'b00)
                    begin
                    next_state <= CHK;
                    end

                 else if (chk_ok == 2'b10)
                    begin
                    next_state <= ADQ;
                    end

                 else
                    begin
                    next_state <= CHK;
                    end

            LCK: if(chk_ok == 2'b10)
                    begin
                    next_state <= ADQ;
                    end

                 else
                    begin
                    next_state <= LCK;
                    end

        endcase
      end

 always @(state)
       begin: salidas
          case(state)
            ADQ: prbs_in_sel <= 1;

            CHK: prbs_in_sel <= 0;

            LCK: prbs_in_sel <= 0;

         endcase
       end

assign o_data = error;


endmodule
