module fir 
#(
	parameter NB_I_DATA ='d9,
	parameter NBF_I_DATA ='d6,
	parameter PARALELLISM_I_KERNEL =31,
	parameter NB_I_KERNEL ='d13,
	parameter NBF_I_KERNEL ='d10,
	parameter NB_O_DATA ='d7,
	parameter NBF_O_DATA ='d6,
	parameter PARALELLISM_O_REG =30,
	parameter NB_O_REG ='d9,
	parameter NBF_O_REG ='d6
)
(
	input 	clk,
	input signed 	[NB_I_DATA-1:0] i_data,
	input  i_reset,
	input signed 	[PARALELLISM_I_KERNEL*NB_I_KERNEL-1:0] i_kernel,
	output signed 	[NB_O_DATA-1:0] o_data,
	output signed 	[PARALELLISM_O_REG*NB_O_REG-1:0] o_reg
);

/*$$$*/

        localparam NB_MULT = NB_I_DATA + NB_I_KERNEL;    //Este seria NB_FR (en la foto)
        localparam NBF_MULT = NBF_I_DATA + NBF_I_KERNEL; // Este seria NBF_FR
        localparam ADD_NB = $clog2(PARALELLISM_I_KERNEL);
        localparam N_REG = PARALELLISM_I_KERNEL-1;
        integer i=0;
        integer j=0;
        integer k=0;

        reg signed [NB_I_DATA-1:0] regresor [N_REG-1:0];
        reg signed [NB_MULT-1:0] mult [PARALELLISM_I_KERNEL-1:0]; //Seria el A


        //Pametros y registros para truncado y saturacion
        localparam NB_O_M = 10;
        localparam NBF_O_M = 6;
        localparam LONG_SUMADOR = NB_O_M+ADD_NB;
        localparam PE_MULT = NB_MULT - NBF_MULT;
        localparam PE_O_M = NB_O_M - NBF_O_M;

        reg signed [LONG_SUMADOR-1:0] sumador;
        wire signed [NB_O_M-1:0] b1 [PARALELLISM_I_KERNEL-1:0];

//        //COMPORTAMIENTO FILTRO FIR


        always @ (posedge clk)
            begin: Desplazamiento

            if(i_reset)
                begin
                for(i=0; i<N_REG ; i=i+1)
                    begin
                        regresor[i] <= {NB_I_DATA{1'b0}};
                    end
                end

            else
            begin

                for(i=0; i < N_REG; i=i+1)
                begin
                    if(i==0)

                    begin regresor[i] <= i_data; end


                    else

                    begin regresor[i] <= regresor[i-1]; end

                end
                end
            end

        always @ (*)
            begin: multiplicacion

            for(j=0; j <= PARALELLISM_I_KERNEL-1; j=j+1)
            begin
                if(j==0)
                begin
                    mult[j] = $signed(i_kernel[0 +: (NB_I_KERNEL)])*i_data;
                end

                else
                begin
                    mult[j] = $signed(i_kernel[(NB_I_KERNEL*j) +: (NB_I_KERNEL)])*regresor[j-1];
                end
            end
            end

            always @ (*)
                begin: Arbol_de_suma

                    if(i_reset)
                         sumador = {(NB_O_M+ADD_NB){1'b0}};

                    else begin
                         sumador = {(NB_O_M+ADD_NB){1'b0}};

                            for (k=0 ; k <= PARALELLISM_I_KERNEL-1 ; k = k+1)
                            begin
                                sumador = sumador + b1[k];
                             end
                    end
                end


//instanciando RYS y SAT
genvar v1;

assign o_reg = {regresor[29],regresor[28],regresor[27],regresor[26],regresor[25],regresor[24],regresor[23],regresor[22],regresor[21],regresor[20],regresor[19],regresor[18],regresor[17],regresor[16],regresor[15],regresor[14],regresor[13],regresor[12],regresor[11],regresor[10],regresor[9],regresor[8],regresor[7],regresor[6],regresor[5],regresor[4],regresor[3],regresor[2],regresor[1],regresor[0]};

generate
for(v1=0;v1<=PARALELLISM_I_KERNEL-1;v1=v1+1)
   begin: proceso_instanciacion_rys
      rys #(
       .NB_I_DATA(NB_MULT),
       .NBF_I_DATA(NBF_MULT),
       .NB_O_DATA (NB_O_M),
       .NBF_O_DATA(NBF_O_M)
       )
      u_rys(
       .clk(clk),
       .i_data(mult[v1]), //Aca es donde se cruzan para dar correctas referencias. VER NOTA ABAJO.
       .o_data(b1[v1])
      );
   end
endgenerate


generate
   begin: proceso_instanciacion_sat
      sat #(
       .NB_I_DATA(LONG_SUMADOR),
       .NBF_I_DATA(NBF_O_M),
       .NB_O_DATA (NB_O_DATA),
       .NBF_O_DATA(NBF_O_DATA)
       )
      u_sat(
       .clk(clk),
       .i_data(sumador), //Aca es donde se cruzan para dar correctas referencias. VER NOTA ABAJO.
       .o_data(o_data)
      );
   end
endgenerate

// fir2 #(
// 		 .NB_I_DATA(NB_I_DATA),
// 		 .NBF_I_DATA(NBF_I_DATA),
// 		 .PARALELLISM_I_KERNEL(PARALELLISM_I_KERNEL),
// 		 .NB_I_KERNEL(NB_I_KERNEL),
// 		 .NBF_I_KERNEL(NBF_I_KERNEL),
// 		 .NB_O_DATA(NB_O_DATA),
// 		 .NBF_O_DATA(NBF_O_DATA)
// )
// u_fir2 (
// 		 .clk(clk),
// 		 .i_data(i_data),
// 		 .i_reset(i_reset),
// 		 .i_kernel(i_kernel),
// 		 .o_data(o_data)
// 		 //.o_data(o_data)
// );


endmodule
