module top_TB();

localparam PARALELLISM_I_SEED =7;

localparam NB_I_KERNEL ='d7;
localparam NBF_I_KERNEL ='d6;
localparam PARALELLISM_I_KERNEL =10;





localparam NB_O_ERROR ='d6;
localparam NBF_O_ERROR ='d6;




localparam NB_O_DATA ='d16;



/*Variables para la apertura de archivos*/
integer testFilei_seed;
integer linesReadedi_seed;
integer inputToi_seed;

integer testFilei_kernel;
integer linesReadedi_kernel;
integer inputToi_kernel;

integer testFilei_valid;
integer linesReadedi_valid;
integer inputToi_valid;

integer testFilei_reset;
integer linesReadedi_reset;
integer inputToi_reset;

integer fileToMatcho_error;
integer linesReadedMatcho_error;
integer dataCompareo_error;

integer fileToMatcho_data_slicer;
integer linesReadedMatcho_data_slicer;
integer dataCompareo_data_slicer;

integer fileToMatcho_data;
integer linesReadedMatcho_data;
integer dataCompareo_data;


/*Entradas y salidas del modulo*/
reg [PARALELLISM_I_SEED-1:0] i_seed;
reg signed [PARALELLISM_I_KERNEL*NB_I_KERNEL-1:0] i_kernel;
reg i_valid;
reg i_reset;
wire signed [NB_O_ERROR-1:0] o_error;
wire o_data_slicer;
wire [NB_O_DATA-1:0] o_data;

/*Seniales internas de vector matching*/
reg signed [NB_O_ERROR-1:0] match_o_error;
wire o_error_assert;
reg match_o_data_slicer;
wire o_data_slicer_assert;
reg  [NB_O_DATA-1:0] match_o_data;
wire o_data_assert;

/*Variables propias del modelo*/

reg clk=0; 
integer i;

/*Generacion de clock*/

always
begin
	 #2 clk = ~clk;
end

initial
begin
	testFilei_seed = $fopen("/home/fer/PPS/TOP_SR/rtl/default/dataToi_seed.dat","r");
	if(testFilei_seed=="NULL")
	begin
		$display("Unable to open file");
		$stop;
	end

	testFilei_kernel = $fopen("/home/fer/PPS/TOP_SR/rtl/default/dataToi_kernel.dat","r");
	if(testFilei_kernel=="NULL")
	begin
		$display("Unable to open file");
		$stop;
	end

	testFilei_valid = $fopen("/home/fer/PPS/TOP_SR/rtl/default/dataToi_valid.dat","r");
	if(testFilei_valid=="NULL")
	begin
		$display("Unable to open file");
		$stop;
	end

	testFilei_reset = $fopen("/home/fer/PPS/TOP_SR/rtl/default/dataToi_reset.dat","r");
	if(testFilei_reset=="NULL")
	begin
		$display("Unable to open file");
		$stop;
	end

	fileToMatcho_error = $fopen("/home/fer/PPS/TOP_SR/rtl/default/dataToo_error.dat","r");
	if(fileToMatcho_error=="NULL")
	begin
		$display("Unable to open file");
		$stop;
	end

	fileToMatcho_data_slicer = $fopen("/home/fer/PPS/TOP_SR/rtl/default/dataToo_data_slicer.dat","r");
	if(fileToMatcho_data_slicer=="NULL")
	begin
		$display("Unable to open file");
		$stop;
	end

	fileToMatcho_data = $fopen("/home/fer/PPS/TOP_SR/rtl/default/dataToo_data.dat","r");
	if(fileToMatcho_data=="NULL")
	begin
		$display("Unable to open file");
		$stop;
	end

end


always @(posedge clk)
begin
	for(i=0;i<PARALELLISM_I_SEED;i=i+1)
	begin
		linesReadedi_seed<= $fscanf(testFilei_seed,"%d",inputToi_seed);
		if ($feof(testFilei_seed))
		begin
			$display("Fin del archivo"); 
			//CLOSE ALL FILES HERE
			$finish;
		end


		i_seed[((i+1))-1-:1]<=(inputToi_seed); 
		end


	for(i=0;i<PARALELLISM_I_KERNEL;i=i+1)
	begin
		linesReadedi_kernel<= $fscanf(testFilei_kernel,"%d",inputToi_kernel);
		if ($feof(testFilei_kernel))
		begin
			$display("Fin del archivo"); 
			//CLOSE ALL FILES HERE
			$finish;
		end


		i_kernel[(NB_I_KERNEL*(i+1))-1-:NB_I_KERNEL]<=(inputToi_kernel); 
		end


		linesReadedi_valid<= $fscanf(testFilei_valid,"%d",inputToi_valid);
		if ($feof(testFilei_valid))
		begin
			$display("Fin del archivo"); 
			//CLOSE ALL FILES HERE
			$finish;
		end


		i_valid<=(inputToi_valid); 
		linesReadedi_reset<= $fscanf(testFilei_reset,"%d",inputToi_reset);
		if ($feof(testFilei_reset))
		begin
			$display("Fin del archivo"); 
			//CLOSE ALL FILES HERE
			$finish;
		end


		i_reset<=(inputToi_reset); 
	linesReadedMatcho_error<= $fscanf(fileToMatcho_error,"%d",dataCompareo_error);

	if ($feof(fileToMatcho_error))
		begin
			$display("Fin del archivo"); 
			$finish;
		end


		match_o_error[NB_O_ERROR-1-:NB_O_ERROR]<=(dataCompareo_error); 
	linesReadedMatcho_data_slicer<= $fscanf(fileToMatcho_data_slicer,"%d",dataCompareo_data_slicer);

	if ($feof(fileToMatcho_data_slicer))
		begin
			$display("Fin del archivo"); 
			$finish;
		end


		match_o_data_slicer<=(dataCompareo_data_slicer); 
	linesReadedMatcho_data<= $fscanf(fileToMatcho_data,"%d",dataCompareo_data);

	if ($feof(fileToMatcho_data))
		begin
			$display("Fin del archivo"); 
			$finish;
		end


		match_o_data[NB_O_DATA-1-:NB_O_DATA]<=(dataCompareo_data); 


end
 
///***ASSERTS***///
assign o_error_assert = (match_o_error==o_error) ? 1'b1 : 1'b0;
assign o_data_slicer_assert = (match_o_data_slicer==o_data_slicer) ? 1'b1 : 1'b0;
assign o_data_assert = (match_o_data==o_data) ? 1'b1 : 1'b0;

 
top#(
	.NB_I_KERNEL 	 	 	(NB_I_KERNEL),
	.NBF_I_KERNEL 	 	 	(NBF_I_KERNEL),
	.NB_O_ERROR 	 	 	(NB_O_ERROR),
	.NBF_O_ERROR 	 	 	(NBF_O_ERROR),
	.NB_O_DATA 	 	 	(NB_O_DATA))
 
 
 	DUT_top ( 
 	.clk          (clk),
	.i_seed 	 	 	(i_seed[PARALELLISM_I_SEED-1:0]),
	.i_kernel 	 	 	(i_kernel[PARALELLISM_I_KERNEL*NB_I_KERNEL-1:0]),
	.i_valid 	 	 	(i_valid),
	.i_reset 	 	 	(i_reset),
	.o_error 	 	 	(o_error[NB_O_ERROR-1:0]),
	.o_data_slicer 	 	 	(o_data_slicer),
	.o_data 	 	 	(o_data[NB_O_DATA-1:0])
 
);
 
endmodule