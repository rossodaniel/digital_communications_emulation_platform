module top(
    input wire CLK,
    output reg [7:0] led,
    input wire [7:0] sw,
    input wire btnc, btnd, btnl, btnr, btnu
    );

    always @ (posedge CLK)
    begin
        if(sw[0] == 0)
        begin
            led[7:0] <= 8'b00000000;
        end
        else
        begin
            if (btnc == 0)
            begin
                led[7:0] <= 8'b10101010;
            end
            else
            begin
                led[7:0] <= 8'b01010101;
            end
        end
    end
endmodule
