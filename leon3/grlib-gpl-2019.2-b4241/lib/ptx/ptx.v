module ptx
#(
	parameter PARALELLISM_I_DATA =9
)
(
	input 	clk,
	input  	[PARALELLISM_I_DATA-1:0] i_data,
	input  i_carga,
	output  o_data
);

/*$$$*/

/*Parámetros y variables de la PRBS*/
integer bfbk= 5'd1;
integer i=0;
//Cálculo del bit de realimentación variable
always@(posedge i_carga)
begin
    case (PARALELLISM_I_DATA)
      5'd7: begin
                  bfbk=1;
               end
      5'd9: begin
                  bfbk=4;
               end
      5'd11: begin
                  bfbk=2;
               end
      5'd15: begin
                  bfbk=1;
               end
      5'd20: begin
                  bfbk=17;
               end
      5'd23: begin
                  bfbk=5;
               end
      5'd31: begin
                  bfbk=3;
               end
      default: begin
                  bfbk=1;
               end
    endcase
end
//Registros y señales
reg [PARALELLISM_I_DATA-1:0] prbstx = {PARALELLISM_I_DATA{1'b0}};
reg serial_out = 1'b0;
reg fbk=0;

/*Comportamiento*/
   //assign fbk = prbstx[0]^prbstx[bfbk];
   assign o_data =  prbstx[0];//serial_out;

   always@(posedge clk)
   begin: fbk_registrado
      //fbk = prbstx[0]^prbstx[bfbk];
      if (~i_carga)
         fbk = prbstx[0]^prbstx[bfbk];
      else
         fbk = fbk;
   end

   always @(posedge clk)
   begin: desplazamiento
      if (i_carga) begin
         for (i=0;i<PARALELLISM_I_DATA;i=i+1) begin
            prbstx[i] <= i_data[(PARALELLISM_I_DATA-1)-i];
         end
         //fbk <= i_data[6]^i_data[6-bfbk];
         //fbk = fbk;
         //serial_out <= i_data[6];
      end
      else begin //else if (<clock_enable>) begin
         //fbk <= prbstx[0]^prbstx[bfbk];
         //serial_out <= prbstx[0];
         prbstx <= {fbk, prbstx[PARALELLISM_I_DATA-1:1]};
         //prbstx <= {prbstx[PARALELLISM_I_DATA-2:1], fbk};

      end
   end
endmodule
