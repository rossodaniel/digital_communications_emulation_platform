module rys 
#(
	parameter NB_I_DATA ='d24,
	parameter NBF_I_DATA ='d20,
	parameter NB_O_DATA ='d23,
	parameter NBF_O_DATA ='d20
)
(
	input 	clk,
	input signed 	[NB_I_DATA-1:0] i_data,
	output signed 	[NB_O_DATA-1:0] o_data
);

/*$$$*/
//Redondeo
      
        localparam SHIFT_RED = NBF_I_DATA - NBF_O_DATA - 1; 
        localparam NB_PT = (NB_I_DATA - (NBF_I_DATA - NBF_O_DATA)); //post truncado
  
        reg signed [NB_I_DATA:0] a1; 
        wire signed [NB_I_DATA-1:0] redondeo;
        reg signed [NB_PT:0] a2;
        reg signed [NB_O_DATA-1:0] b;

assign redondeo = 1'b1 << SHIFT_RED;
         
        always @ (*)
        begin : Redondeo
            begin 
                a1 = i_data + redondeo;
                a2 = a1[(NB_I_DATA) -: (NB_PT+1)]; //VER ESTA SENTENCIA           
                               
            end
        
        end

//Parámetros saturacion
localparam PE_I = NB_I_DATA - NBF_I_DATA; //Also could be NBF_O_DATA because its same as NBF_I_DATA.
localparam PE_O = NB_O_DATA - NBF_O_DATA;
localparam DIF = PE_I - PE_O;
localparam EXT_SIGNO = PE_O-PE_I; //Se usa cuando PE_I < PE_O

//Variables y registros:
reg [DIF-1:0] region;
reg [NB_O_DATA-1:0] sat;
wire [NB_O_DATA-1:0] sat2;
reg sdet0, sdet1, signo; //Detectores de la salida del sumador.


//---------------------------------------- COMPORTAMIENTO ---------------------------------------//
    always@(*)
    begin:saturacion   
        if(PE_I == PE_O) //PARTE ENTERA IGUALES EJ. 10.7 y 7.4
            begin
            sat = a2;
            end
       
        else if(PE_I<PE_O) //PARTE ENTERA DE SALIDA MAYOR A LA DE ENTRADA EJ. 10.8 y 9.5
            begin
            
            signo = a2[NB_PT-1];
            sat = {signo,{EXT_SIGNO{signo}}, a2[(NB_O_DATA-2-EXT_SIGNO):0]};            
            
            end      
        
        else //CASO PE_I > PE_O
            begin
            
            region = a2[NB_PT-2 : NB_O_DATA-1];
            sdet0 = &region;
            sdet1 = |region;
            signo = a2[NB_PT-1];
            
            if(signo) begin
                if(~sdet0)
                    sat = {signo, {NB_O_DATA-1{1'b0}}};//-1 porque dejo el signo.
                else
                    sat = {signo, a2[NB_O_DATA-2:0]};//No tomo el signo, lo agrego antes, por eso -2.
                end
            else   
                begin
                    if(sdet1)
                        sat = {signo, {NB_O_DATA-1{1'b1}}};//-1 porque dejo el signo.
                    else
                        sat = {signo, a2[NB_O_DATA-2:0]};//No tomo el signo, lo agrego antes, por eso -2.
                end
            end
        end    
    
assign sat2 = sat;
assign o_data = sat2;


endmodule
