module sat 
#(
	parameter NB_I_DATA ='d27,
	parameter NBF_I_DATA ='d20,
	parameter NB_O_DATA ='d22,
	parameter NBF_O_DATA ='d20
)
(
	input 	clk,
	input signed 	[NB_I_DATA-1:0] i_data,
	output signed 	[NB_O_DATA-1:0] o_data
);

/*$$$*/
//Parámetros:
localparam PE_I = NB_I_DATA - NBF_I_DATA; //Also could be NBF_O_DATA because its same as NBF_I_DATA.
localparam PE_O = NB_O_DATA - NBF_O_DATA;
localparam DIF = PE_I - PE_O;
localparam EXT_SIGNO = PE_O-PE_I; //Se usa cuando PE_I < PE_O
localparam NB_PT = (NB_I_DATA - (NBF_I_DATA - NBF_O_DATA)); //post truncado
//Variables y registros:


reg [DIF-1:0] region;
reg [NB_O_DATA-1:0] sat;
reg sdet0, sdet1, signo; //Detectores de la salida del sumador.
reg signed [NB_O_DATA:0] a2;

//---------------------------------------- COMPORTAMIENTO ---------------------------------------//

 always@(*)
    begin:saturacion   
        if(PE_I == PE_O) //PARTE ENTERA IGUALES EJ. 10.7 y 7.4
            begin
            sat = i_data;
            end
       
        else if(PE_I<PE_O) //PARTE ENTERA DE SALIDA MAYOR A LA DE ENTRADA EJ. 10.8 y 9.5
            begin
            
            signo = i_data[NB_I_DATA-1];
            sat = {signo,{EXT_SIGNO{signo}}, i_data[(NB_O_DATA-2-EXT_SIGNO):0]};            
            
            end      
        
        else //CASO PE_I > PE_O
            begin
            
            region = i_data[NB_I_DATA-2 : NB_O_DATA-1];
            sdet0 = &region;
            sdet1 = |region;
            signo = i_data[NB_I_DATA-1];
            
            if(signo) begin
                if(~sdet0)
                    sat = {signo, {NB_O_DATA-1{1'b0}}};//-1 porque dejo el signo.
                else
                    sat = {signo, i_data[NB_O_DATA-2:0]};//No tomo el signo, lo agrego antes, por eso -2.
                end
            else   
                begin
                    if(sdet1)
                        sat = {signo, {NB_O_DATA-1{1'b1}}};//-1 porque dejo el signo.
                    else
                        sat = {signo, i_data[NB_O_DATA-2:0]};//No tomo el signo, lo agrego antes, por eso -2.
                end
            end
        end    

assign o_data = sat;

endmodule



