------------------------------------------------------------------------------
--  This file is a part of the GRLIB VHDL IP LIBRARY
--  Copyright (C) 2003 - 2008, Gaisler Research
--  Copyright (C) 2008 - 2014, Aeroflex Gaisler
--  Copyright (C) 2015 - 2019, Cobham Gaisler
--
--  This program is free software; you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation; either version 2 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program; if not, write to the Free Software
--  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
-----------------------------------------------------------------------------
-- Package: 	apb_example
-- Entity:	apb_example
-- File:	apb_example.vhd
-- Author:	I-tera SAS
-- Description:	TESt
------------------------------------------------------------------------------



library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
library grlib;
use grlib.amba.all;
use grlib.devices.all;
library gaisler;

use gaisler.misc.all;

entity apb_example is

  generic (
    pindex   : integer := 0;
    paddr    : integer := 0;
    pmask    : integer := 16#fff#);
  port (
    rst    : in  std_ulogic;
    clk    : in  std_ulogic;
    apbi   : in  apb_slv_in_type;
    apbo   : out apb_slv_out_type;
    enable : in  std_logic);

end;



architecture rtl of apb_example is
  constant REVISION : integer := 0;
  constant DEVICE_ID : integer := 0;
-- VENCOR_ID = various contributions
  -- DEVICE_ID = 0 (device ID does not matter)
  -- VERSION_ID = 1

  constant PCONFIG : apb_config_type := (

      0 => ahb_device_reg (VENDOR_CONTRIB, DEVICE_ID, 0, REVISION, 0),
      --0 => ahb_device_reg (1, 1, 0, 0, 0),
      1 => apb_iobar(paddr, pmask));

  type registers is record

    reg : std_logic_vector(31 downto 0);

  end record;
    
  signal count:    std_logic_vector(31 downto 0):= "00000000000000000000000000000000";
  signal temporal: std_logic_vector(31 downto 0):= "00000000000000000000000000000000";
  signal r, rin : registers;

  begin
    comb : process(rst, r, apbi,enable)
      variable readdata : std_logic_vector(31 downto 0);
          variable v        : registers;
          -- read register
              begin    v := r;    -- read register
              readdata := (others => '0');

              case apbi.paddr(4 downto 2) is
                  when "000" => readdata := r.reg(31 downto 0);
                  when others => null;
              end case;

            -- write registers
            if (apbi.psel(pindex) and apbi.penable and apbi.pwrite) = '1' then

              case apbi.paddr(4 downto 2) is
                  when "000" => v.reg := apbi.pwdata;
                  when others => null;
              end case;
            end if;


            -- system reset
            if rst = '0' then v.reg :=  (others => '0'); end if;

            rin <= v;
            --apbo.prdata <= readdata; -- drive apb read bus
            --apbo.prdata      <= "101010101010101";
              apbo.prdata      <= temporal;
            
            --if(enable='1') then
            --apbo.prdata      <= (apbi.pwdata OR ("00000000000000000000000000001111"));
            --else
            --apbo.prdata      <= (apbi.pwdata OR ("00000000000000000000000011110000"));
            --end if;
            
                -- if(enable='1') then
                        --apbo.prdata      <= (apbi.pwdata OR ("00000000000000000000000000001111"));
                        --else
                        --apbo.prdata      <= (apbi.pwdata OR ("00000000000000000000000011110000"));
                        --end if;
            
               
                                                                            
            end process;

            apbo.pirq <= (others => '0');         -- No IRQ
            apbo.pindex <= pindex;                -- VHDL generic
            apbo.pconfig <= PCONFIG;              -- Config constant

            -- registers

            regs : process(clk)
            begin
                if rising_edge(clk) then 
                temporal <= temporal + 1; 
                count <= count + 1;
                r <= rin; 
                end if;
                
                 if(enable='1') then
                               temporal      <= (("00000000000000000000000000000000"));
                               end if;
                               
            end process;

            -- boot message

            -- pragma translate_off
                bootmsg : report_version
                generic map ("apb_example" & tost(pindex) &": Example core rev " & tost(REVISION));

            -- pragma translate_on
            end;
