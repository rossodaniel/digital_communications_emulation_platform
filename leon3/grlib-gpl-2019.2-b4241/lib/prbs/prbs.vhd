------------------------------------------------------------------------------
--  This file is a part of the GRLIB VHDL IP LIBRARY
--  Copyright (C) 2003 - 2008, Gaisler Research
--  Copyright (C) 2008 - 2014, Aeroflex Gaisler
--  Copyright (C) 2015 - 2019, Cobham Gaisler
--
--  This program is free software; you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation; either version 2 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program; if not, write to the Free Software
--  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
-----------------------------------------------------------------------------
-- Package: top
-- Entity:	top
-- File:	prbs.vhd
-- Author:	I-tera SAS
-- Description:	TOP_INSTANCE
------------------------------------------------------------------------------



library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
library grlib;
use grlib.amba.all;
use grlib.devices.all;
library gaisler;

use gaisler.misc.all;

entity prbs is

  generic (
    pindex   : integer := 0;
    paddr    : integer := 0;
    pmask    : integer := 16#fff#);
  port (
    -- carga    : in  std_ulogic;
    clk    : in  std_ulogic;
    apbi   : in  apb_slv_in_type;
    apbo   : out apb_slv_out_type;
    enable : in  std_logic);

end;



architecture rtl of prbs is

--Instancia prbs

 component ptx

   generic (
     PARALELLISM_I_DATA : integer :=7 );
   port (
     clk      : in  std_ulogic;
     i_data   : in  std_logic_vector(6 downto 0);
     i_carga  : in  std_logic;
     o_data   : out std_logic);


 end component;

----Instancia mapper
 component mpr

   generic (
     NB_O_DATA : integer :=2 );
   port (
     clk      : in  std_ulogic;
     i_data   : in  std_logic;
     i_reset  : in  std_logic;
     i_valid  : in  std_logic;
     o_data   : out std_logic_vector(1 downto 0));


 end component;

----Instancia fir del canal
 component fch

   generic (
     NB_I_DATA: integer := 2;
 	  NBF_I_DATA: integer  := 1;
 	  PARALELLISM_I_KERNEL: integer :=9;
 	  NB_I_KERNEL: integer  := 7;
 	  NBF_I_KERNEL: integer := 6;
 	  NB_O_DATA: integer := 9;
 	  NBF_O_DATA:integer  := 6);
   port (
     clk      : in  std_ulogic;
     i_data   : in  std_logic_vector(1 downto 0);
     i_reset  : in  std_logic;
     i_kernel : in  std_logic_vector(62 downto 0);
     o_data   : out std_logic_vector(8 downto 0));


 end component;

 ----Instancia ecualizador

 component equ

    generic (
      NB_I_DATA: integer := 9;
      NBF_I_DATA: integer := 6;
      NB_O_ERROR: integer := 6;
      NBF_O_ERROR: integer := 6);
    port(
      clk      :in std_ulogic;
      i_data   :in std_logic_vector(8 downto 0);
      i_reset  :in std_logic;
      o_error  :out std_logic_vector(5 downto 0);
      o_data_slicer :out std_logic);

end component;

component chk

    generic (
      NB_O_DATA: integer := 16);
    port(
      clk      :in std_ulogic;
      i_data   :in std_logic;
      i_reset  :in std_logic;
      i_valid  :in std_logic;
      o_data  :out std_logic_vector(15 downto 0));

end component;

-- component top
--
--       generic(PARALELLISM_I_SEED: integer :=7;
--       	      PARALELLISM_I_KERNEL: integer :=9;
--       	      NB_I_KERNEL: integer :=7;
--       	      NBF_I_KERNEL: integer := 6;
--       	      NB_O_ERROR: integer := 6;
--       	      NBF_O_ERROR: integer :=6;
--       	      NB_O_DATA: integer := 16);
--       port(
--         clk         :in std_ulogic;
--       	i_seed      :in std_logic_vector(6 downto 0);
--         i_kernel    :in std_logic_vector(62 downto 0);
--       	i_valid     :in std_logic;
--       	i_reset     :in std_logic;
--       	o_error     :out std_logic_vector(5 downto 0);
--       	o_data      :out std_logic_vector(15 downto 0));
--
-- end component;


  signal kernel: std_logic_vector(62 downto 0);
  signal out_to_next_component: std_logic_vector(31 downto 0) ;
  --signal out_to_next_component_not: std_logic_vector(31 downto 0) ;

  signal out_bit_equ: std_logic_vector(31 downto 0);
  signal out_bit_equ_not: std_logic_vector(31 downto 0);

  signal out_error_equ: std_logic_vector(31 downto 0) ;
  signal out_error_bit_rate: std_logic_vector(31 downto 0) ;
  signal out_to_apb: std_logic_vector(31 downto 0) ;


  constant valid: std_logic := '1';

  constant REVISION : integer := 0;
  constant DEVICE_ID : integer := 0;
-- VENCOR_ID = various contributions
  -- DEVICE_ID = 0 (device ID does not matter)
  -- VERSION_ID = 1

  constant PCONFIG : apb_config_type := (

      0 => ahb_device_reg (VENDOR_CONTRIB, DEVICE_ID, 0, REVISION, 0),
      --0 => ahb_device_reg (1, 1, 0, 0, 0),
      1 => apb_iobar(paddr, pmask));

  type registers is record

    reg : std_logic_vector(31 downto 0);

  end record;

     signal enable_reg: std_logic;
     signal seleccion: std_logic_vector(2 downto 0);

  -- signal count:    std_logic_vector(31 downto 0):= "00000000000000000000000000000000";
  -- signal temporal: std_logic_vector(31 downto 0):= "00000000000000000000000000000000";
  signal s,kl,kh,rw : registers;
----------------------------------------------------------------------------------------------
  begin
----------------------------------------------------------------------------------------------

      -- top0: top
      -- generic map(PARALELLISM_I_SEED => 7,
      -- 	          PARALELLISM_I_KERNEL => 9,
      -- 	          NB_I_KERNEL => 7,
      -- 	          NBF_I_KERNEL => 6,
      -- 	          NB_O_ERROR => 6,
      -- 	          NBF_O_ERROR => 6,
      -- 	          NB_O_DATA => 16)
      -- port map(  clk => clk,
      -- 	         i_seed =>s.reg(6 downto 0),
      --            i_kernel => kernel,
      -- 	         i_valid  => valid,
      -- 	         i_reset  => enable_reg,
      -- 	         o_error  => out_error_equ(5 downto 0),
      -- 	         o_data   => out_error_bit_rate(15 downto 0));


   ptx0 : ptx
   generic map(PARALELLISM_I_DATA => 7)
   port map(clk => clk,
            i_data=>s.reg(6 downto 0),
            i_carga=>enable_reg,
            o_data =>out_to_next_component(29));


   mpr0 : mpr
   generic map(NB_O_DATA => 2)
   port map(clk => clk,
            i_data => out_to_next_component(29),
            i_reset => enable_reg,
            i_valid => valid,
            o_data => out_to_next_component(28 downto 27));

   fch0 : fch
   generic map(NB_I_DATA => 2,
               NBF_I_DATA => 1,
               PARALELLISM_I_KERNEL => 9,
               NB_I_KERNEL => 7,
               NBF_I_KERNEL => 6,
               NB_O_DATA => 9,
               NBF_O_DATA => 6)
   port map(clk => clk,
           i_data => out_to_next_component(28 downto 27),
           i_reset => enable_reg,
           i_kernel => kernel,
           o_data => out_to_next_component(26 downto 18));

   equ0: equ
   generic map(NB_I_DATA => 9,
               NBF_I_DATA => 6,
               NB_O_ERROR => 6,
               NBF_O_ERROR => 6)
   port map(clk => clk,
            i_data => out_to_next_component(26 downto 18),
            i_reset => enable_reg,
            o_error => out_error_equ(5 downto 0),
            --o_data_slicer => out_to_next_component(15));
            o_data_slicer => out_bit_equ(0));

  chk0: chk
  generic map(NB_O_DATA => 16)
  port map(clk => clk,
           --i_data => out_to_next_component(29),
           i_data =>  out_bit_equ_not(0),
           i_reset => enable_reg,
           i_valid => valid,
           o_data  =>out_error_bit_rate(15 downto 0));


      out_bit_equ_not <= not(out_bit_equ);
      apbo.prdata <= out_to_apb;


-------------------------------------------------------------------------------
    comb : process(s,kl,kh,apbi,enable_reg)
      -- variable readdata : std_logic_vector(31 downto 0);
          variable v,k1,k2,rr     : registers;
          -- read register
              begin    v := s;    -- read register
                       k1 := kl;
                       k2 := kh;
                       rr := rw;

            -- write registers
            if (apbi.psel(pindex) and apbi.penable and apbi.pwrite) = '1' then



              -- if(enable_reg = '1') then
              --
              --   out_to_apb(8 downto 0) <= "000000001";
              --
              -- else

              case apbi.paddr(4 downto 2) is
                  when "000" => v.reg := apbi.pwdata;
                                --out_to_apb(17 downto 0) <= "111000111000111000";

                  when "001" => k1.reg := apbi.pwdata;
                                --out_to_apb(17 downto 0) <= "111000111111000111";

                  when "010" => k2.reg := apbi.pwdata;
                                --out_to_apb(17 downto 0) <= "110000000111000111";

                  when "011" => rr.reg := apbi.pwdata;
                                --out_to_apb(17 downto 0) <= "110000000111000111";

                  when others => null;
              end case;



          -- end if;
          end if;


            -- system reset
            -- if rst = '0' then v.reg :=  (others => '0'); end if;

            s <= v;
            kl <= k1;
            kh <= k2;
            rw <= rr;


              --apbo.prdata      <= temporal;
            end process;


            kernel <= kh.reg(30 downto 0)&kl.reg; --Concateno la parte alta y la parte baja del kernel


            apbo.pirq <= (others => '0');         -- No IRQ
            apbo.pindex <= pindex;                -- VHDL generic
            apbo.pconfig <= PCONFIG;              -- Config constant


            regs : process(clk)
            begin

             if(rising_edge(clk)) then
                enable_reg <= enable;

             end if;
            end process;


            sel : process(clk)
            begin

             if(rising_edge(clk)) then

                  seleccion <= rw.reg(2 downto 0);
            end if;
            end process;

            out_to_apb             <= out_error_equ      when seleccion = "000" else
                                      out_error_bit_rate when seleccion = "001" else
                                      out_bit_equ when seleccion = "010" else
                                      "00000000000000000000000000000001";

            -- boot message

            -- pragma translate_off
                bootmsg : report_version
                generic map ("prbs" & tost(pindex) &": Example core rev " & tost(REVISION));

            -- pragma translate_on
            end;
