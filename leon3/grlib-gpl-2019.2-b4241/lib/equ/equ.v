module equ 
#(
	parameter NB_I_DATA ='d9,
	parameter NBF_I_DATA ='d6,
	parameter NB_O_ERROR ='d6,
	parameter NBF_O_ERROR ='d6
)
(
	input 	clk,
	input signed 	[NB_I_DATA-1:0] i_data,
	input  i_reset,
	output signed 	[NB_O_ERROR-1:0] o_error,
	output  o_data_slicer
);

/*$$$*/
localparam NB_O_DATA_FIR = 7;
localparam NBF_O_DATA_FIR = 6;
localparam PARALELLISM_I_KERNEL_FIR = 31;
localparam NB_I_KERNEL_FIR = 13;
localparam NBF_I_KERNEL_FIR = 10;

//Parámetros del SLICER del ecualizador:
localparam NB_I_DATA_SLR = NB_O_DATA_FIR;
localparam NBF_I_DATA_SLR = NBF_O_DATA_FIR;
localparam NB_O_SLR_ERROR = 6;
localparam NBF_O_SLR_ERROR = 6;

//Parámetros de la LMS del ecualizador:
localparam NB_I_LMS_ERROR = NB_O_SLR_ERROR;
localparam NBF_I_LMS_ERROR = NBF_O_SLR_ERROR;
//localparam NB_I_LMS_ERROR = NB_I_ERROR;
//localparam NBF_I_LMS_ERROR = NBF_I_ERROR;
localparam PARALELLISM_O_DATA_LMS = PARALELLISM_I_KERNEL_FIR;
localparam NB_O_DATA_LMS = NB_I_KERNEL_FIR;
localparam NBF_O_DATA_LMS = NBF_I_KERNEL_FIR;

//Variables, señales y registros:
wire signed [NB_O_DATA_FIR-1:0] s_data_fir_to_slicer;
wire signed [(PARALELLISM_I_KERNEL_FIR*NB_I_KERNEL_FIR)-1:0] s_kernel_lms_to_fir;
wire signed [NB_O_SLR_ERROR-1:0] s_error_slr_to_lms;

//---------------------------------------- COMPORTAMIENTO ---------------------------------------//
//Instanciación FIR:
      fir #(
           .NB_I_DATA(NB_I_DATA),
           .NBF_I_DATA(NBF_I_DATA),
           .PARALELLISM_I_KERNEL(PARALELLISM_I_KERNEL_FIR),
           .NB_I_KERNEL(NB_I_KERNEL_FIR),
           .NBF_I_KERNEL(NBF_I_KERNEL_FIR),
           .NB_O_DATA(NB_O_DATA_FIR),
           .NBF_O_DATA(NBF_O_DATA_FIR)
      )
      u_fir(
           .clk(clk),
           .i_data(i_data),
           .i_reset(i_reset),
           .i_kernel(s_kernel_lms_to_fir),
           .o_data( s_data_fir_to_slicer)
      );
//Fin instanciación FIR.

//Instanciación SLICER:
      slr #(
	       .NB_I_DATA(NB_I_DATA_SLR),
	       .NBF_I_DATA(NBF_I_DATA_SLR),
	       .NB_O_ERROR(NB_O_SLR_ERROR),
	       .NBF_O_ERROR(NBF_O_SLR_ERROR)
      )
      u_slr(
           .clk(clk),
           .i_data(s_data_fir_to_slicer),
           .i_reset(i_reset),
           .o_data(o_data_slicer),
           .o_error(s_error_slr_to_lms)
           //.o_error(o_error)

      );
//Fin instanciación SLR.

//Instanciación LMS:
      lmsins #(
	       .NB_I_DATA(NB_I_DATA),
	       .NBF_I_DATA(NBF_I_DATA),
	       .NB_I_ERROR(NB_I_LMS_ERROR),
	       .NBF_I_ERROR(NBF_I_LMS_ERROR),
	       .PARALELLISM_O_DATA(PARALELLISM_O_DATA_LMS),
	       .NB_O_DATA(NB_O_DATA_LMS),
	       .NBF_O_DATA(NBF_O_DATA_LMS)
	       //.PARALELLISM_O_REG(PARALELLISM_O_REG_LMS),
           //.NB_O_REG(NB_O_REG_LMS),
           //.NBF_O_REG(NBF_O_REG_LMS)
      )
      u_lmsins(
           .clk(clk),
	       .i_data(i_data),
	       .i_error(s_error_slr_to_lms),
	       .i_reset(i_reset),
	       .o_data(s_kernel_lms_to_fir)

      );

assign o_error = s_error_slr_to_lms;
endmodule
