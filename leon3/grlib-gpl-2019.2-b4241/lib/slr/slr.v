module slr 
#(
	parameter NB_I_DATA ='d7,
	parameter NBF_I_DATA ='d6,
	parameter NB_O_ERROR ='d6,
	parameter NBF_O_ERROR ='d6
)
(
	input 	clk,
	input signed 	[NB_I_DATA-1:0] i_data,
	input  i_reset,
	output  o_data,
	output signed 	[NB_O_ERROR-1:0] o_error
);

/*$$$*/
reg signed [1:0] d;
reg out;
wire [NB_O_ERROR-1:0] sust;
wire [NB_O_ERROR-1:0] error;
reg s_out_to_o_data;
reg [NB_O_ERROR-1:0] s_error_to_o_error;

localparam NBF_D = 1; 
localparam aline = NBF_O_ERROR - NBF_D;

always@(*)
begin: reset_combinacional
    if(i_reset) begin
        s_out_to_o_data = 0;
        s_error_to_o_error = {NB_O_ERROR{1'b0}};
    end
    else begin
        s_out_to_o_data = out;
        s_error_to_o_error = error;
    end
end

always@(*)
begin: slicer
        if(i_data >= 0)
            begin  
            out = 1'b1;
            d = 2'b01; 
            end 
        else 
            begin
            out = 1'b0;
            d = 2'b11;
            end
end

assign sust = d << aline;  
assign error = i_data - sust;
//Asignación de salidas:
assign o_data = s_out_to_o_data;
assign o_error = s_error_to_o_error;


endmodule
