-makelib ies/xil_defaultlib \
  "../../../../vivado/mig/user_design/rtl/ui/mig_7series_v4_0_ui_top.v" \
  "../../../../vivado/mig/user_design/rtl/ui/mig_7series_v4_0_ui_wr_data.v" \
  "../../../../vivado/mig/user_design/rtl/ui/mig_7series_v4_0_ui_rd_data.v" \
  "../../../../vivado/mig/user_design/rtl/ui/mig_7series_v4_0_ui_cmd.v" \
  "../../../../vivado/mig/user_design/rtl/ip_top/mig_7series_v4_0_memc_ui_top_std.v" \
  "../../../../vivado/mig/user_design/rtl/ip_top/mig_7series_v4_0_mem_intfc.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_byte_lane.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_mc_phy.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_phy_ocd_samp.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_phy_prbs_rdlvl.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_of_pre_fifo.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_phy_init.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_phy_dqs_found_cal_hr.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_phy_wrlvl_off_delay.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_phy_ocd_data.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_phy_ocd_edge.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_poc_tap_base.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_poc_edge_store.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_phy_tempmon.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_calib_top.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_prbs_gen.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_phy_oclkdelay_cal.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_if_post_fifo.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_phy_ocd_cntlr.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_phy_ocd_lim.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_phy_ocd_po_cntlr.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_phy_ocd_mux.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_poc_meta.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_mc_phy_wrapper.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_skip_calib_tap.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_phy_wrlvl.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_phy_rdlvl.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_poc_top.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_poc_cc.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_phy_dqs_found_cal.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_phy_ck_addr_cmd_delay.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_poc_pd.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_byte_group_io.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_phy_wrcal.v" \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_phy_4lanes.v" \
  "../../../../vivado/mig/user_design/rtl/clocking/mig_7series_v4_0_clk_ibuf.v" \
  "../../../../vivado/mig/user_design/rtl/clocking/mig_7series_v4_0_iodelay_ctrl.v" \
  "../../../../vivado/mig/user_design/rtl/clocking/mig_7series_v4_0_infrastructure.v" \
  "../../../../vivado/mig/user_design/rtl/clocking/mig_7series_v4_0_tempmon.v" \
  "../../../../vivado/mig/user_design/rtl/controller/mig_7series_v4_0_rank_mach.v" \
  "../../../../vivado/mig/user_design/rtl/controller/mig_7series_v4_0_bank_cntrl.v" \
  "../../../../vivado/mig/user_design/rtl/controller/mig_7series_v4_0_rank_common.v" \
  "../../../../vivado/mig/user_design/rtl/controller/mig_7series_v4_0_mc.v" \
  "../../../../vivado/mig/user_design/rtl/controller/mig_7series_v4_0_round_robin_arb.v" \
  "../../../../vivado/mig/user_design/rtl/controller/mig_7series_v4_0_arb_select.v" \
  "../../../../vivado/mig/user_design/rtl/controller/mig_7series_v4_0_bank_common.v" \
  "../../../../vivado/mig/user_design/rtl/controller/mig_7series_v4_0_bank_queue.v" \
  "../../../../vivado/mig/user_design/rtl/controller/mig_7series_v4_0_rank_cntrl.v" \
  "../../../../vivado/mig/user_design/rtl/controller/mig_7series_v4_0_arb_mux.v" \
  "../../../../vivado/mig/user_design/rtl/controller/mig_7series_v4_0_col_mach.v" \
  "../../../../vivado/mig/user_design/rtl/controller/mig_7series_v4_0_arb_row_col.v" \
  "../../../../vivado/mig/user_design/rtl/controller/mig_7series_v4_0_bank_compare.v" \
  "../../../../vivado/mig/user_design/rtl/controller/mig_7series_v4_0_bank_state.v" \
  "../../../../vivado/mig/user_design/rtl/controller/mig_7series_v4_0_bank_mach.v" \
  "../../../../vivado/mig/user_design/rtl/ecc/mig_7series_v4_0_fi_xor.v" \
  "../../../../vivado/mig/user_design/rtl/ecc/mig_7series_v4_0_ecc_gen.v" \
  "../../../../vivado/mig/user_design/rtl/ecc/mig_7series_v4_0_ecc_buf.v" \
  "../../../../vivado/mig/user_design/rtl/ecc/mig_7series_v4_0_ecc_merge_enc.v" \
  "../../../../vivado/mig/user_design/rtl/ecc/mig_7series_v4_0_ecc_dec_fix.v" \
-endlib
-makelib ies/xil_defaultlib \
  "../../../../vivado/mig/user_design/rtl/phy/mig_7series_v4_0_ddr_phy_top.vhd" \
  "../../../../vivado/mig/user_design/rtl/mig_mig_sim.vhd" \
  "../../../../vivado/mig/user_design/rtl/mig.vhd" \
-endlib
-makelib ies/xil_defaultlib \
  glbl.v
-endlib

