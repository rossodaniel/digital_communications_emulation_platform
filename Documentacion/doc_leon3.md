# Instanciacion de simulador de sistemas de comunicaciones digitales como bloque ASIC custom

## Introducción y alcance

En este documento se detalla como realizar la implementación de un bloque ASIC como periférico de un procesador LEON3 utilizando bus AMBA.

Para la implementación en hardware se utiliza una placa Digilent Nexys Video, el bloque ASIC en este caso consta de un sistema de comunicaciones digitales, compuesto por diversos bloques tales como: prbs, mapper, canal(filtro fir), ecualizador y por ultimo un checker, los cuales son descriptos en lenguaje verilog, e instanciados en un bloque general descripto en vhdl llamado "top.vhd" en el que realizan las conexiones necesarias para el correcto funcionamiento entre los distintos bloques.

Por otra parte y a modo de testing, se genera un programa en C que mediante punteros a direcciones de memoria donde se encuentra la instancia del bloque en cuestión, se definen los valores de entrada que recibirá el bloque instanciado, a su vez otro puntero se utiliza para realizar la selección de la salida que se desea obtener, esta salida depende del valor que tenga asignado dicho puntero. El sistema de selección se explica con mayor profundidad mas adelante.

Como debuger se utiliza GRMON en su versión de evaluación.

## Requerimientos previos

Para el desarrollo del proyecto se deben satisfacer los siguientes requerimientos:

>Ubuntu 18.04 LTS o similar - Testeado en Ubuntu 18.04.1 LTS

>Vivado 2017.1 - 2018.1 - Testeado en ambas

>Digilent Nexys Video Board o similar

## Instalación de la placa en Vivado

Para instalar la Nexys Video en Vivado, se deben copiar los archivos de la carpeta board_files a la carpeta boards/board_files ubicada donde se encuentra instalado Vivado (por defecto: /opt/Xilinx/Vivado/2017.1/data/boards/board_files).

También se deben instalar los drivers USB para vincular la FPGA con Linux, para esto se debe ejecutar como súper usuario el script ./install_drivers ubicado en &lt;vivado_setup&gt;/data/xicom/cable_drivers/lin64/install_script/install_drivers/

~~~
$ pwd
/opt/Xilinx/Vivado/2017.1/data/xicom/cable_drivers/lin64/install_script/install_drivers
$ sudo ./install_drivers
INFO: Installing cable drivers.
INFO: Script name = ./install_drivers
INFO: HostName = T480s-5
INFO: Current working dir = /opt/Xilinx/Vivado/2017.1/data/xicom/cable_drivers/lin64/install_script/install_drivers
INFO: Kernel version = 4.15.0-70-generic.
INFO: Arch = x86_64.
USB udev file exists and will not be updated.
INFO: Driver installation successful.
CRITICAL WARNING: Cable(s) on the system must be unplugged then plugged back in order for the driver scripts to update the cables.

~~~

Para probar que la instalación se ha realizado correctamente, se añade un archivo de test y las constraints de la Nexys Video.
A fin de ponerlo en funcionamiento, se debe crear un proyecto nuevo y seguir los suguientes pasos:

1. File - New project - Seleccionar ubicación y nombre del proyecto
2. Seleccionar RTL project.

3. Incluir el archivo test.v ubicado en la carpeta vivado_test
4. Añadir las constraints (nexys.xdc) ubicada en la misma ruta que el archivo del ítem anterior.

5. Buscar la opción "Nexys Video" en el menú desplegable que se presenta. Si dicha opción aparece en la lista, significa que Vivado ha reconocido los archivos correctamente. Caso contrario verificar si la instalación se realizó correctamente.

6. Finalizar la creación del proyecto.

7. Generar el Bitstream y programar la FPGA (tener en cuenta que la placa debe estar conectada a la PC utilizando el puerto prog).

8. Luego de poner en alto el SW[0], si al oprimir el BTNC los siete leds cambian de estado, el proyecto está funcionando.

## Leon 3

### Inicializacion del proyecto

Dentro de la carpeta grlib se encuentra extraído el proyecto Leon3, estos archivos fueron descargados desde el siguiente link:  <https://www.gaisler.com/index.php/downloads/leongrlib>.

Para comenzar a trabajar con el proyecto, se debe ejecutar el makefile ubicado en el directorio:  `/leon3/grlib-gpl-2019.2-b4241/designs/leon3-digilent-nexys-video/`

Para realizar el makefile, hay que ubicarse en la dirección que se detalla arriba y ejecutar el comando `make vivado`.

Cuando el proyecto comienza a construirse, se mostrará el siguiente error:

~~~
$ make vivado

Starting DRC Task
Command: report_drc (run_mandatory_drcs) for: opt_checks
INFO: [DRC 23-27] Running DRC with 8 threads
ERROR: [DRC INBB-3] Black Box Instances: Cell 'mig_gen.gen_mig.ddrc/MCB_inst' of type 'mig_gen.gen_mig.ddrc/MCB_inst/mig' has undefined contents and is considered a black box.  The contents of this cell must be defined for opt_design to complete successfully.
report_drc (run_mandatory_drcs) completed successfully
INFO: [Project 1-461] DRC finished with 1 Errors
INFO: [Project 1-462] Please refer to the DRC report (report_drc) for more information.
ERROR: [Vivado_Tcl 4-78] Error(s) found during DRC. Opt_design not run.

Time (s): cpu = 00:00:01 ; elapsed = 00:00:00.47 . Memory (MB): peak = 2130.461 ; gain = 64.031 ; free physical = 6849 ; free virtual = 12328
19 Infos, 6 Warnings, 4 Critical Warnings and 2 Errors encountered.
opt_design failed
ERROR: [Common 17-39] 'opt_design' failed due to earlier errors.
$
~~~

Para resolverlo, se debe lanzar la gui de vivado, abrir el proyecto ubicado en grlib-gpl-2019.2-b4241/designs/leon3-digilent-nexys-video/vivado/leon3-digilent-nexys-video/ ir a la sección IPSources y actualizar Memory Interface Generator (Mig 7 series).

![Mig][img1]

[img1]: ../Documentacion/mig.png "Actualización mig"

Una vez solucionado este problema, volver a lanzar el comando `make vivado`, cuando ya se ha generado el bitstream se debe grabar el mismo en la placa. Este paso se hace mediante la gui de vivado.

## Bus AMBA

AMBA (Advanced Microcontroller Bus Architecture) es un bus de interconexión que se usa ampliamente como bus on-chip para diferentes aplicaciones. Es decir permite conectar, controlar y gestionar periféricos y bloques diseñados para SoC (System-on-chip).
El estándar AMBA define tres tipos de BUS:

- Advanced High-performance Bus (AHB): está diseñado para comunicar componentes de alto rendimiento como procesadores y módulos con altas frecuencias de reloj.

- Advanced Peripheral Bus (APB): está diseñado para comunicar periféricos de bajo consumo y de menores prestaciones.

- Advanced System Bus (ASB): es una versión simplificada de AHB.
Este bus será utilizado en su versión APB para la comunicación del contador con el resto de los periféricos.

## Creación de un bloque ASIC custom

La librería GRLIB utilizada para la instanciación del LEON 3 junto al BUS AMBA y provee una estructura de ejemplo para cada uno de los tipos mencionados en el apartado anterior. Para la creación de este proyecto en particular se utiliza la estructura APB en modo slave, en esta configuración todos los registros del bloque son de lectura/escritura.

A continuación se detallan las variables y registros de interés del bloque:

pindex, paddr y pmask se utilizan para la configuración del bus APB, se definen en la estructura del bloque a instanciar pero son configurados desde el top.
- pconfig: en esta sección se pueden configurar todos los parámetros a excepción del VENCOR_ID que debe definirse como VENDOR_CONTRIB, a fin de poder utilizar la versión de evaluación del GRMON para hacer debug.

- apbi: esta estructura está compuesta por varios elementos, a los fines prácticos se detallarán los estrictamente necesarios para el funcionamiento:
  - apbi.paddr: lectura de la dirección de memoria, esto permite conocer si el mensaje recibido se debe procesar o no.

  - apbi.pwdata: lectura de los datos escritos en el bus. Esto puede ser utilizado para comunicación entre dispositivos, pero en este caso se utiliza para recibir datos y parámetros necesarios para nuestro bloque.

- apbo: al igual que en el apartado anterior, se detalla sólo el más importante:
  - apbo.prdata: escritura de datos sobre el bus. Esta estructura se utiliza para las diversas salidas del bloque en cuestión.

## Ubicación del archivo

Para que el bloque personalizado pueda ser accesible desde las instancias del proyecto, el nombre de la carpeta que contiene los archivos debe estar incluida dentro del archivo `libs.txt` ubicado en `../grlib-gpl-2019.2-b4241/lib/grlib/`. En este ejemplo el subdirectorio es llamado “prbs”. Por otra parte, dentro de esta última carpeta existe el archivo “vhdlsyn.txt” el contiene el nombre de todos los archivos a incluir dentro de esta subcarpeta.

Como este archivo se  compone por varios bloques, cada componente debe tener su respectiva carpeta y estar incluido en el archivo de texto.


## Instanciación dentro del TOP

Para lograr instanciar un bloque personalizado dentro del bus AMBA, en primer lugar se necesita crear un componente en el archivo top del LEON con el mismo nombre, parámetros e interfaces que posee el bloque que fue creado en el apartado anterior.
Paso siguiente se debe realizar la instanciación mediante un bloque generate, teniendo en cuenta que la dirección definida en los parámetros pindex y paddr definirán la posición inicial del bloque custom dentro del bus.

En las siguientes lineas de código extraídas del archivo top "leon3mp.vhd" se detalla lo explicado anteriormente:

Declaración del componente:

~~~
component prbs

  generic (
    pindex   : integer := 0;
    paddr    : integer := 0;
    pmask    : integer := 16#fff#);
  port (
    clk    : in  std_ulogic;
    apbi   : in  apb_slv_in_type;
    apbo   : out apb_slv_out_type;
    enable : in  std_logic);

end component;
~~~

Instanciacion del componente:

~~~
prbs0 : if CFG_GRGPIO_ENABLE /= 0  generate
prbs0 : prbs
generic map( pindex => 13, paddr => 13)
port map(clkm, apbi,apbo(13),sw(0));

end generate;
~~~

***NOTA:***
Cabe aclarar que en este caso, el componente prbs que se declara, es en realidad el top. Falta realizar algunos cambios para colocar su nombre correctamente.

##Bloque a instanciar

El bloque que es instanciado en el top, es el sistema de comunicaciones nombrado anteriormente.

Es necesario generar una especie de "cascara" en vhdl, la cual lleva los parámetros del bus AMBA (pindex, paddr, etc), para lograr realizar correctamente la instanciacion en el LEON 3 y que todo funcione correctamente. Dentro de dicha cascara, se instancian los diferentes bloques que componen el sistema de comunicación digital.

![instancia][img2]

[img2]: ../Documentacion/Instancias.png "Instancias dentro del top"

En la imagen anterior se observa la cascara que es instanciada en LEON 3, y a sus vez las sub instanicias que componen al bloque prbs(top).

Donde:

- prbs = top sistema de comunicaciones (Falta realizar cambio de nombre en archivo e instancia)
- ptx = prbs
- mpr = mapper
- fch = filtro fir como canal
- equ = ecualizador compuesto por filtro fir, lms y o_data_slicer
- chk = checker

### Entradas y salidas

#### Entradas

Para este caso particular se cuenta con dos entradas de datos, la semilla que ingresa a la prbs, de 7 bits, y por otro lado el kernel del filtro FIR el cual conforma el canal del sistema, el mismo posee 63 bits para una configuración de 9 taps de 7 bits cada uno. Debido a que tanto para entrada y salida de datos se utiliza el bus de datos APB (Advanced Peripheral Bus) el cual posee un total de 32 bits. Se implementan multiplexores para poder manejar datos mayores a esta cantidad de bits.

En el siguiente fragmento de codigo extraido del archivo ***prbs.vhd*** se puede observar el multiplexor que se implementa para manejar los datos de entrada los cuales son enviados desde un programa en C.

~~~
case apbi.paddr(4 downto 2) is
    when "000" => v.reg := apbi.pwdata;
    when "001" => k1.reg := apbi.pwdata;
    when "010" => k2.reg := apbi.pwdata;
    when "011" => rr.reg := apbi.pwdata;
    when others => null;
end case;
~~~

donde ***v*** es la variable encargada de recibir el valor de la semilla, mientras que el kernel del canal se divide en ***k1*** y ***k2***, es decir, es dividido en lsb y msb respectivamente.

La manera utilizada para seleccionar diferentes casos, es mediante punteros a diferentes posiciones de memoria, como se muestra en el siguiente fragmento de codigo en C, el cual se ocupa de cargar los valores de entrada:

~~~
volatile unsigned int *seed = (int *)(0x80000D00);
volatile unsigned long int *kernell = (long int *)(0x80000D00 + 0x4);
volatile unsigned long int *kernelh = (long int *)(0x80000D00 + 0x8);
volatile unsigned int *readwrite = (int *)(0x80000D00 + 0xC);
~~~

Se puede observar que los distintos punteros apuntan a una misma dirección de memoria pero siendo desplazados en 4 bytes. Es mediante este desplazamiento que se consigue seleccionar los distintos casos de entrada.

Luego de esto se debe dar un valor a dichos punteros, los cuales son enviados al bloque que se ha instanciado.

#### Salidas

Es posible seleccionar la salida deseada desde el programa en C, mediante un puntero, el mismo se declara de la igual forma que los demás, en este caso dicho puntero es el que se denomina ***readwrite*** en el código presentado anteriormente. Y se corresponde con la variable ***rr*** en el case del archivo en vhdl que también es detallado anteriormente.

En este caso, la salida del multiplexor que controla las señales de salida, depende del valor asociado a dicho puntero, es decir, el selector de salida se relaciona con el valor que tenga el puntero en ese momento.

En este caso, el multiplexor se describe utilizando un when else.

~~~
out_to_apb   <= out_error_equ      when seleccion = "000" else
                out_error_bit_rate when seleccion = "001" else
                "00000000000000000000000000000001";
~~~

Donde ***out_to_apb*** es un vector que luego es asignado a la salida total del sistema, ***seleccion*** es un vector de 3 componentes que se encarga de leer los 3 bits menos significativos de ***rr*** (variable donde se encuentra el valor del puntero selector de salidas) y ***out_error_equ*** y ***out_error_bit_rate*** son algunas de las salidas de interés del sistema.

En este caso se utilizan solamente dos salidas, pero debido a que el bus tiene 32 bits, el vector selección puede poseer hasta 32 bits, es decir, se podría seleccionar una cantidad de hasta 2^32 salidas diferentes, mientras que las mismas no superen los 32 bits cada una, en caso contrario, se debería diseñar un sistema de multiplexado y concatenación de salidas para lograr una salida mayor a 32 bits.

## Grabación del proyecto en la FPGA

Una vez que han sido realizados los pasos anteriores, el proyecto se encuentra listo para generar la síntesis y la implementación. Todos estos pasos pueden ser realizados automáticamente oprimiendo el botón generate bitstream en la GUI de Vivado. Cuando ya se ha generado el archivo .bit, se debe grabar en la FPGA.

## Programación y debug del LEON utilizando GRMON

El archivo `hello_word.c` que se ubica dentro de la carpeta "software" en el directorio raíz del proyecto, contiene el  programa para leer y escribir la porción del bus de memoria asociada al bloque que ha sido instanciado.
Para lograr esto, se definen punteros a enteros de tipo volatile, los cuales son inicializados con la dirección de memoria asignada en la instanciación de el bloque bloque con el cual se viene trabajando dentro el top (en este caso particular es x80000D00).
La compilación de este código fuente se realiza con BCC, el cual es un cross-compiler para LEON3 basado en las herramientas de compilación GNU.
A la compilación normal se agregan los flags -qnano (permite el uso de funciones fprintf() y fscanf()) y -mcpu=leon3 el cual define la arquitectura del CPU.

Una vez cross-compilado el proyecto, se debe cambiar el cable de la placa al conector UART, ubicarse en la carpeta donde se encuentra el GRMON y lanzarlo mediante el siguiente comando:


`$ sudo grmon -uart /dev/ttyUSB0 -cginit 0x11111111`


Las opción -cginit se encarga de resetear y habilitar a todos los periféricos a fin de poder ser debugueados.

Para cargar el programa previamente compilado, se ejecuta la siguiente sentencia dentro del grmon:

`$ load ../leon3/software/hello.elf`

y mediante la sentencia `run` se indica al programa que arranque su ejecución.

Previamente puede ser necesario conocer las direcciones asignadas a los periféricos, esto se consigue utilizando el comando `info sys`.
En la imagen siguiente, se realiza la ejecución de dicho comando, y se pueden observar los distintos bloques instanciados y periféricos, el bloque llamado `adev11` (lleva este nombre debido a que se utiliza una versión de evaluación) es el bloque que fue instanciado mediante los pasos explicados en este documento, y se indica que ocupa una posición de memoria que va desde x80000D00 hasta x80000E00 en el bus de direcciones de memoria.

![info sys][img3]

[img3]: ../Documentacion/infosys.png "Info sys en grmon"
